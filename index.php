<?php 

session_name("loginUsuario");
session_start();
$_SESSION["pub_user"] = "VPEREZ";
$_SESSION["modu_id"] = 2;

$desarrollo = true;
if($desarrollo)
{
	$cec_essentials = "http://sistemas.cecltda.cl/cec/ditorium_desarrollo/cec_essentials/";
	$api_general = "http://sistemas.cecltda.cl/cec/ditorium_desarrollo/api/";
}
else
{
	$cec_essentials = "http://sistemas.cecltda.cl/cec/ditorium/cec_essentials/";
	$api_general = "http://sistemas.cecltda.cl/cec/ditorium/api/";
}

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>CEC COBRANZA</title>

		<link href="<?php echo $cec_essentials; ?>plugins/fontawesome/css/all.min.css" rel="stylesheet">
		<link href="<?php echo $cec_essentials; ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo $cec_essentials; ?>plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet">

		<link href="<?php echo $cec_essentials; ?>plugins/select2/select2.css" rel="stylesheet">
		<link href="<?php echo $cec_essentials; ?>plugins/jQuery-Validation-Engine/css/validationEngine.jquery.css" rel="stylesheet">
		<link href="<?php echo $cec_essentials; ?>plugins/DataTables/datatables.min.css" type="text/css" rel="stylesheet" />		
		<link href="<?php echo $cec_essentials; ?>plugins/JQuery_zTree_v3.5.13/css/zTreeStyle/zTreeStyle.css" type="text/css" rel="stylesheet" >	
		<link href="<?php echo $cec_essentials; ?>plugins/datetimepicker/jquery.datetimepicker.min.css" type="text/css" rel="stylesheet" />	
		
		<link href="<?php echo $cec_essentials; ?>design/css/theme.css" rel="stylesheet">
	</head>
	<body>
		<div class="sidebar">
			<ul>
				<li><a class="menu-option" data-fn="funcion_vacia" data-url="views/home.php"><i class="fa fa-home" aria-hidden="true"></i>Inicio</a></li>
				<li><a class="menu-option" data-fn="inicia_modulo_cobranza" data-url="views/cobranza/submenu.php"><i class="fas fa-dollar-sign" aria-hidden="true"></i>Cobranza</a></li>
				<li><a class="menu-option" data-fn="funcion_vacia" data-url="#"><i class="fa fa-power-off" aria-hidden="true"></i>Cerrar Sesión</a></li>
			</ul>
		</div>	
		<div id="adorno_superior"></div>
		<div class="inner-body">
			<div id="upper-body">
				<div id="upper-inner-body"></div>
				<span class="txtc" id="titulo-modulo">Cobranza</span>
				<span class="txtc" id="titulo-seccion"></span>
				<span class="txtr" id="nombre_usuario">Usuario CEC</span>
			</div>
			<div id="main-container"></div>
		</div>

		<div id="dialog_vacio" style="display:none;"></div>
		<div id="dialog_creacion" style="display:none;"></div>
		<div id="error_message_dialog" style="display:none">
			<span id="error_message_icon"></span>
			<span id="error_message_text"></span>
		</div>

	</body>
	<script src="<?php echo $cec_essentials; ?>plugins/jquery-ui/jquery.js"></script>	    
	<script src="<?php echo $cec_essentials; ?>plugins/jquery-ui/jquery-ui.min.js"></script>

	<script src="<?php echo $cec_essentials; ?>plugins/DataTables/datatables.min.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/select2/select2.min.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/jQuery-Validation-Engine/js/jquery.validationEngine.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/jQuery-Validation-Engine/js/languages/jquery.validationEngine-es.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/JQuery_zTree_v3.5.13/js/jquery.ztree.all-3.5.min.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/JQuery_zTree_v3.5.13/js/jquery.ztree.exhide-3.5.min.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/datetimepicker/jquery.datetimepicker.full.min.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/jQuery-Rut/jquery.Rut.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/numeral.js/min/numeral.min.js"></script>
	<script src="<?php echo $cec_essentials; ?>plugins/numeral.js/min/locales/es.min.js"></script>
	<script src="<?php echo $cec_essentials; ?>app.js"></script>


	<script src="controllers/menu.js"></script>
	<script src="controllers/cobranza.js"></script>
	<script src="controllers/generar_reposicion.js"></script>
	<script src="controllers/asignacion.js"></script>
	<script src="controllers/asignacion_reposicion.js"></script>
	<script src="controllers/prejudicial_judicial.js"></script>
	<script src="controllers/corte.js"></script>
	<script src="controllers/reposicion.js"></script>
	
	<script type="text/javascript">

		var cec_essentials = "<?php echo $cec_essentials; ?>";
		var api_general = "<?php echo $api_general; ?>";
		var sw_desarrollo = "<?php echo $desarrollo; ?>";

		$(document).ready(function()
		{
			app_init_plugins();	

			$("#main-container").load("views/home.php",{},function(){});	

		});

  	</script>
</html>