var dt_asignacion = undefined;
var dt_asignacion_reposicion_cuentas = undefined;
var id_check_actual_revisado = undefined;
var asignacion_reposicion_doc_actual = 0;

var data_asignaciones = Array();

var filtro_cuenta_asignacion_reposicion = "";
var filtro_tarifa_asignacion_reposicion = "TODO";
var filtro_motivo_asignacion_reposicion = "TODO";
var filtro_proceso_asignacion_reposicion = "TODO";
var filtro_ruta_asignacion_reposicion = "TODO";
var filtro_asignado_asignacion_reposicion = "TODO";

function inicia_listar_reposiciones_asignaciones()
{
	iniciar_datatable_listar_asignaciones_reposiciones();
	var permiso_new = getJSON(api_general+"usuarios/get_permiso",{id_ui:'asignacion_reposicion_listar_agregar'});
	if(permiso_new)
	{
		$("#botonera_superior").append('<button type="button" id="asignacion_reposicion_listar_agregar" data-url="views/cobranza/ingresar_asignacion_reposicion.php" class="btn btn-xl btn-accion" ><i class="fas fa-plus"></i> Asignar Reposición</button>');
	}
	var permiso_reasignar = getJSON(api_general+"usuarios/get_permiso",{id_ui:'asignacion_reposicion_listar_reasignar'});
	if(permiso_reasignar)
	{
		$("#botonera_superior").append('<button type="button" id="asignacion_reposicion_listar_reasignar" data-url="views/cobranza/ingresar_asignacion_reposicion.php" class="btn btn-xl btn-accion" ><i class="fas fa-share-square"></i> Reasignar Reposición</button>');
	}	
}

function iniciar_datatable_listar_asignaciones_reposiciones()
{
	var columnDefs = [
	{
	    // "targets": [2],
	    // "className": "txtr",
	    // "width": "4%"
	}];
	var filtro = {};
	filtro.fecha = $("#asignacion_reposicion_listar_tabla_filtro").val();
	if(dt_asignacion != undefined)
	{
		dt_asignacion.destroy();
	}
	var permiso_ver = getJSON(api_general+"usuarios/get_permiso",{id_ui:'asignacion_reposicion_listar_ver'});
	var permiso_reposicion = getJSON(api_general+"usuarios/get_permiso",{id_ui:'asignacion_reposicion_listar_reposicion'});
	var permiso_anular = getJSON(api_general+"usuarios/get_permiso",{id_ui:'asignacion_reposicion_listar_anular'});
	dt_asignacion = iniciar_datatable_basica("asignacion_reposicion_listar_tabla",api_general+"cbz_asignacion/sp_reposicion?"+$.param(filtro),function(valor){
		var id_asignacion = valor[4];
		var vigencia = valor[3];

		if(vigencia=="S")
		{
			valor[3]= "<span class='label label-success'>Vigente</span>";
		}
		else
		{
			valor[3]= "<span class='label label-danger'>Anulada</span>";
		}

		var btnes = "";
		if(permiso_ver)
		{
			btnes += "<button type='button' class='btn btn-xs btn-accion' id='asignacion_reposicion_listar_ver' title='Ver' data-url='views/cobranza/asignar_reposicion.php' value='"+id_asignacion+"'><i class='far fa-file-pdf'></i></button>";;
		}
		if(vigencia=="S")
		{
			if(permiso_reposicion)
			{
				btnes += "<button type='button' class='btn btn-xs btn-accion' id='asignacion_reposicion_listar_reposicion' title='Ingresar Reposiciones' data-url='views/cobranza/ingresar_reposicion.php' value='"+id_asignacion+"'><i class='fas fa-wrench'></i></button>";;
			}
			if(permiso_anular)
			{
				var puede_anular = getJSON(api_general+"cbz_asignacion/valida_anulacion_asignacion",{codigo:id_asignacion});
				if(puede_anular.respuesta)
				{
					btnes += "<button type='button' class='btn btn-xs btn-accion' id='asignacion_reposicion_listar_anular' title='Anular Asignaciones' data-url='views/cobranza/anular_reposicion.php' value='"+id_asignacion+"'><i class='fas fa-times'></i></button>";;
				}
			}
		}

		valor[4] =btnes;
		return valor;
	},undefined,undefined,true,columnDefs);
	$("#asignacion_reposicion_listar_tabla_wrapper .ui-corner-tr").append("<div class='datatable_title_custom'><label>Listado de Asignaciones y Estado de Reposicion</label></div>");	
}

$(document).on("change","#asignacion_reposicion_listar_tabla_filtro",function()
{
	iniciar_datatable_listar_asignaciones_reposiciones();
});


$(document).on("click","#asignacion_reposicion_listar_agregar",function()
{
	contenidoSubPrincipal($(this).data("url"));
	$(' #asignacion_reposicion_ingresar_cuenta_tabla th:nth-child(8)').remove();
	filtro_asignado_asignacion_reposicion = "SA";
	inicia_asignacion_reposicion();
	asignacion_reposicion_ingresar_actualizar_tabla();
	// $(' #asignacion_ingresar_cuenta_tabla td:nth-child(8)').hide();	
});

$(document).on("click","#asignacion_reposicion_listar_anular",function()
{
	contenidoSubPrincipal($(this).data("url"));
	inicia_asignacion_reposicion_anulacion($(this).val());
	// $(' #asignacion_ingresar_cuenta_tabla td:nth-child(8)').hide();
});

$(document).on("click","#asignacion_reposicion_listar_reasignar",function()
{
	contenidoSubPrincipal($(this).data("url"));
	filtro_asignado_asignacion_reposicion = "A";
	inicia_asignacion_reposicion();
	asignacion_reposicion_ingresar_actualizar_tabla();
});


$(document).on("click","#asignacion_reposicion_listar_reposicion",function()
{
	contenidoSubPrincipal($(this).data("url"));
	inicia_reposicion($(this).val());
});

function inicia_asignacion_reposicion()
{
	asignacion_reposicion_doc_actual = 0;
	$("#asignacion_reposicion_ingresar_cabecera_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#asignacion_reposicion_ingresar_filtros_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});	
	$("#asignacion_reposicion_ingresar_contratista").select2({ data: getJSON(api_general+"contratista/s2") });
	$("#asignacion_reposicion_ingresar_operador").select2({ data: getJSON(api_general+"operador_contratista/s2",{"contratista":0})});
	
	$("#asignacion_reposicion_ingresar_contratista").on("change",function(e)
	{
		var id_contr = $("#asignacion_reposicion_ingresar_contratista").val();
		$("#asignacion_reposicion_ingresar_operador").select2({ data: getJSON(api_general+"operador_contratista/s2",{"contratista":id_contr})});	
		$("#asignacion_reposicion_ingresar_operador").select2('val','');
	});

	$("#asignacion_reposicion_ingresar_operador").on("change",function(e)
	{
		var codigo = $("#asignacion_reposicion_ingresar_operador").val();
		if(codigo!='')
		{		
			var puede_asignar = getJSON(api_general+"cbz_asignacion/valida_asignacion",{codigo:codigo});
			if(!puede_asignar.respuesta)
			{
				abrir_dialog_message("El móvil <br>"+$("#asignacion_reposicion_ingresar_operador").select2("data").text+"<br> Tiene asignaciones pendientes por lo que no se pueden asignar mas hasta que se completen sus asignaciones previas","alerta");
				$("#asignacion_reposicion_ingresar_operador").select2('val',"");
			}
		}
	});


	var select2_procesos = getJSON(api_general+"proceso/s2");
	if(select2_procesos==undefined)
	{
		select2_procesos = Array();
	}
	select2_procesos.push({id:"TODO",text:"Todos",data:{}});
	$("#asignacion_reposicion_ingresar_filtro_proceso").select2({ data: select2_procesos });


	var new_data = Array();
	new_data = getJSON(api_general+"ruta/s2",{"proceso":0});
	new_data.push({id:"TODO",text:"Todas",data:{}});
	$("#asignacion_reposicion_ingresar_filtro_ruta").select2({ data: new_data});
	
	$("#asignacion_reposicion_ingresar_filtro_proceso").on("change",function(e)
	{
		var new_data = Array();
		var id_proceso = $("#asignacion_reposicion_ingresar_filtro_proceso").val();
		if(id_proceso!="TODO")
		{		
			new_data = getJSON(api_general+"ruta/s2",{"proceso":id_proceso});
			new_data.push({id:"TODO",text:"Todas",data:{}});
			$("#asignacion_reposicion_ingresar_filtro_ruta").prop('disabled',false);
			$("#asignacion_reposicion_ingresar_filtro_ruta").select2({ data: new_data});	
			$("#asignacion_reposicion_ingresar_filtro_ruta").select2('val','TODO');
		}
		else
		{
			new_data.push({id:"TODO",text:"Todas",data:{}});
			$("#asignacion_reposicion_ingresar_filtro_ruta").select2({ data: new_data});	
			$("#asignacion_reposicion_ingresar_filtro_ruta").select2('val','TODO');
			$("#asignacion_reposicion_ingresar_filtro_ruta").prop('disabled',true);

		}
	});	

	filtro_cuenta_asignacion_reposicion = "";
	filtro_tarifa_asignacion_reposicion = "TODO";
	filtro_motivo_asignacion_reposicion = "TODO";
	filtro_proceso_asignacion_reposicion = "TODO";
	filtro_ruta_asignacion_reposicion = "TODO";
	$("#asignacion_reposicion_ingresar_filtro_proceso").select2("val","TODO");
	$("#asignacion_reposicion_ingresar_filtro_ruta").select2("val","TODO");

	data_asignaciones = Array();
	var data = getJSON(api_general+"cbz_asignacion/datos_proceso_reposicion");
	$.each(data,function(i,val)
	{
		var item = {};
		item.ncuenta = val[0];
		item.cliente = val[1];
		item.tarifa = val[2];
		item.motivo = val[3];
		item.ruta = val[4];
		item.proceso = val[5];
		item.id_asignado = val[6];
		item.id_registro = val[7];
		item.nombre_asignado = val[8];
		item.id_proceso = val[9];
		item.id_ruta = val[10];
		item.asignando = false;
		data_asignaciones.push(item);
	});

	asignacion_reposicion_ingresar_actualizar_tabla();

}

$(document).on("change",".asignacion_reposicion_ingresar_check_asignar",function()
{
	id_check_actual_revisado = $(this).val();
	if($(this).data("asignado")!='')
	{
		if($(this).prop('checked'))
		{	
			var nom_asignado = asignacion_reposicion_ingresar_get_nombre_asignado(id_check_actual_revisado);
			abrir_dialog_message("Está reasignando una cuenta ya asignada a:<br>"+nom_asignado+"<br> ¿está seguro?","alerta",[{text:"Aceptar", click:asignacion_reposicion_ingresar_check_asignar_aceptar},{text:"Cancelar", click:asignacion_reposicion_ingresar_check_asignar_cancelar}]);
		}
	}
	else
	{
		asignacion_reposicion_ingresar_actualizar_item_tabla(id_check_actual_revisado,$(this).prop('checked'));
	}
});

function asignacion_reposicion_ingresar_check_asignar_aceptar()
{
	$("#asignacion_reposicion_ingresar_check_asignar_"+id_check_actual_revisado).prop('checked',true);
	cerrar_dialog_message();
	asignacion_reposicion_ingresar_actualizar_item_tabla(id_check_actual_revisado,$("#asignacion_reposicion_ingresar_check_asignar_"+id_check_actual_revisado).prop('checked'));
}

function asignacion_reposicion_ingresar_check_asignar_cancelar()
{
	$("#asignacion_reposicion_ingresar_check_asignar_"+id_check_actual_revisado).prop('checked',false);
	cerrar_dialog_message();
	asignacion_reposicion_ingresar_actualizar_item_tabla(id_check_actual_revisado,$("#asignacion_reposicion_ingresar_check_asignar_"+id_check_actual_revisado).prop('checked'));

}

$(document).on("click","#asignacion_reposicion_ingresar_guardar",function()
{
	asignacion_reposicion_ingresar_guardar_datos(false);
});

$(document).on("click","#asignacion_reposicion_ingresar_guardar_continuar",function()
{
	asignacion_reposicion_ingresar_guardar_datos(true);
});

$(document).on("click","#asignacion_reposicion_ingresar_volver",function()
{
	abrir_dialog_message("Volverá a la lista de Asignaciones y perderá los datos que no hayan sido guardados.<br> ¿Está seguro?","alerta",[{text:"Aceptar", click:asignacion_reposicion_fn_ingresar_volver},{text:"Cancelar", click:cerrar_dialog_message}]);
});

function asignacion_reposicion_fn_ingresar_volver()
{
	contenidoSubPrincipal($("#asignacion_reposicion_submenu_listar").data("url"));
	inicia_listar_reposiciones_asignaciones();
	cerrar_dialog_message();
}


function asignacion_reposicion_ingresar_guardar_datos(continuar)
{
	if($("#asignacion_reposicion_ingresar_cabecera_form").validationEngine('validate'))
	{
		datos = {};
		if(asignacion_reposicion_doc_actual!=0)
		{
			datos.id_asignacion = asignacion_reposicion_doc_actual;
		}
		datos.contratista = $("#asignacion_reposicion_ingresar_contratista").val();
		datos.movil = $("#asignacion_reposicion_ingresar_operador").val();
		var items = Array();
		$.each(data_asignaciones,function(i,val)
		{
			if(val.asignando)
			{
				items.push(val.id_registro);
			}
		});
		datos.items = items;

		if(items.length!=0)
		{
		    abrir_dialog_message("Guardando Datos...","loading",{});
			$.post
			(
				api_general+"cbz_asignacion/cu_reposicion",
				datos,
				function(data)
				{
					asignacion_reposicion_doc_actual = data;
		    		cerrar_dialog_message();
		    		abrir_dialog_message("Datos Guardados Correctamente","aceptar");

		    		if(!continuar)
		    		{    			
			    		contenidoSubPrincipal($("#asignacion_reposicion_submenu_listar").data("url"));
						inicia_listar_reposiciones_asignaciones();
		    		}
		    		else
		    		{
		    			$("#asignacion_reposicion_ingresar_contratista").prop("disabled",true);
		    			$("#asignacion_reposicion_ingresar_operador").prop("disabled",true);
		    		}
				}
				, "json"
			)
			.fail(function(response) {
		    	cerrar_dialog_message();
		    	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
			});
		}
		else
		{
			abrir_dialog_message("Debe asignar al menos una cuenta antes de guardar.","alerta");
		}
	}
}

$(document).on("click","#asignacion_reposicion_listar_ver",function()
{
	window.open("documentos/asignacion.php?codigo="+$(this).val());
});


$(document).on("change","#asignacion_reposicion_ingresar_filtro_asignadas",function()
{
	asignacion_reposicion_ingresar_actualizar_tabla();
});

function asignacion_reposicion_ingresar_actualizar_tabla()
{
	if(dt_asignacion_reposicion_cuentas != undefined)
	{
		dt_asignacion_reposicion_cuentas.destroy();
	}	
	$("#asignacion_reposicion_ingresar_cuenta_tabla> tbody tr").remove();
	$.each(data_asignaciones,function(i,val)
	{
		if(asignacion_reposicion_ingresar_cumple_filtros(val))
		{		
			asignacion_reposicion_ingresar_agregar_item_tabla(val);
		}
	});	
	// dt_asignacion_reposicion_cuentas = iniciar_datatable_basica_vacia("asignacion_reposicion_ingresar_cuenta_tabla",true);
	dt_asignacion_reposicion_cuentas = $('#asignacion_reposicion_ingresar_cuenta_tabla').DataTable( 
    {
    	"paging": false,
    	"searching": true,
        "bSort": false,
    	"info": false,
        "oLanguage": es_leng,
        // "scrollY": "500px",
        // "scrollCollapse": true,
        // "order" : [[0,"DESC"]],
    });
	if(filtro_asignado_asignacion_reposicion=="A")
	{
		$("#asignacion_reposicion_ingresar_cuenta_tabla_wrapper .ui-corner-tr").html("<div class='datatable_title_custom'><label>Listado de Cuentas Asignadas para Reposición</label><button class='btn btn-xs btn-info pull-right' id='asignacion_reposicion_ingresar_asignar_visibles'>Asignar Visibles</button></div>");
	}
	else
	{
		$("#asignacion_reposicion_ingresar_cuenta_tabla_wrapper .ui-corner-tr").html("<div class='datatable_title_custom'><label>Listado de Cuentas a Asignar para Reposición</label><button class='btn btn-xs btn-info pull-right' id='asignacion_reposicion_ingresar_asignar_visibles'>Asignar Visibles</button></div>");
	}    

}

$(document).on("click","#asignacion_reposicion_ingresar_asignar_visibles",function()
{
	var nuevo_array = Array();
	$.each(data_asignaciones,function(i,val)
	{
		if(asignacion_reposicion_ingresar_cumple_filtros(val))
		{

			val.asignando = true;
		}
		nuevo_array.push(val);
	});
	data_asignaciones = nuevo_array;
	asignacion_reposicion_ingresar_actualizar_tabla();
});


function asignacion_reposicion_ingresar_agregar_item_tabla(val)
{
	var row = "<tr>";
	row += "<td>"+val.ncuenta+"</td>";
	row += "<td><input type='text' class='col-xs-12' value='"+val.cliente+"' readonly></td>";
	row += "<td><input type='text' class='col-xs-12' value='"+val.tarifa+"' readonly></td>";
	var motiv = "Reposicion";
	row += "<td><input type='text' class='col-xs-12' value='"+motiv+"' readonly></td>";
	row += "<td><input type='text' class='col-xs-12' value='"+val.ruta+"' readonly></td>";
	row += "<td><input type='text' class='col-xs-12' value='"+val.proceso+"' readonly></td>";

	if(val.asignando)
	{
		row += "<td><input class='form-check-input asignacion_reposicion_ingresar_check_asignar' type='checkbox' data-asignado='"+val.id_asignado+"' value='"+val.id_registro+"' id='asignacion_reposicion_ingresar_check_asignar_"+val.id_registro+"' style='width: 100%;' checked='checked'></td>";
	}
	else
	{
		row += "<td><input class='form-check-input asignacion_reposicion_ingresar_check_asignar' type='checkbox' data-asignado='"+val.id_asignado+"' value='"+val.id_registro+"' id='asignacion_reposicion_ingresar_check_asignar_"+val.id_registro+"' style='width: 100%;'></td>";
	}
	if(filtro_asignado_asignacion_reposicion=="A")
	{
		if(val.id_asignado!="")
		{
			row += "<td><button class='btn btn-xs btn-info' value='"+val.id_registro+"' id='asignacion_reposicion_ingresar_info_asignado'><i class='fas fa-info-circle'></i></button></td>";
		}
		else
		{
			row += "<td></td>";
		}
	}
	row += "</tr>";
	agregarFilaATabla("asignacion_reposicion_ingresar_cuenta_tabla",row);
}

$(document).on("click","#asignacion_reposicion_ingresar_info_asignado",function()
{
	var id_registro = $(this).val();
	var registro = "-Sin Datos-";

	$.each(data_asignaciones,function(i,val)
	{
		if(val.id_registro==id_registro)
		{

			registro = val.nombre_asignado;
		}
	});

	abrir_dialog_message("Asignado a: <br><strong>"+registro+"</strong>","info");

});


function asignacion_reposicion_ingresar_actualizar_item_tabla(id_cambiar, checked)
{
	var nuevo_array = Array();
	$.each(data_asignaciones,function(i,val)
	{
		if(val.id_registro==id_cambiar)
		{

			val.asignando = checked;
		}
		nuevo_array.push(val);
	});
	data_asignaciones = nuevo_array;
}

function asignacion_reposicion_ingresar_get_nombre_asignado(id_registro)
{
	var ret = "-No Name-";
	$.each(data_asignaciones,function(i,val)
	{
		if(val.id_registro==id_registro)
		{
			ret = val.nombre_asignado;
		}
	});
	return ret;
}

// -----------------------------------------------


$(document).on("click","#asignacion_reposicion_ingresar_filtro_filtrar",function()
{
	if($("#asignacion_reposicion_ingresar_filtros_form").validationEngine('validate'))
	{
		filtro_cuenta_asignacion_reposicion = $("#asignacion_reposicion_ingresar_filtro_cuenta").val();
		filtro_tarifa_asignacion_reposicion = $("#asignacion_reposicion_ingresar_filtro_tarifa").val();
		filtro_motivo_asignacion_reposicion = $("#asignacion_reposicion_ingresar_filtro_motivo").val();
		filtro_proceso_asignacion_reposicion = $("#asignacion_reposicion_ingresar_filtro_proceso").val();
		filtro_ruta_asignacion_reposicion = $("#asignacion_reposicion_ingresar_filtro_ruta").val();
		asignacion_reposicion_ingresar_actualizar_tabla();

	}
});

$(document).on("click","#asignacion_reposicion_ingresar_filtro_limpiar",function()
{
	asignacion_reposicion_ingresar_filtros_limpiar();
	asignacion_reposicion_ingresar_actualizar_tabla();
});

function asignacion_reposicion_ingresar_filtros_limpiar()
{
	$("#asignacion_reposicion_ingresar_filtro_cuenta").val("");
	$("#asignacion_reposicion_ingresar_filtro_tarifa").val("TODO");
	$("#asignacion_reposicion_ingresar_filtro_motivo").val("TODO");
	$("#asignacion_reposicion_ingresar_filtro_proceso").select("val","TODO");
	$("#asignacion_reposicion_ingresar_filtro_ruta").select("val","TODO");
	$("#asignacion_reposicion_ingresar_filtro_asignadas").val("TODO");

	filtro_cuenta_asignacion_reposicion = "";
	filtro_tarifa_asignacion_reposicion = "TODO";
	filtro_motivo_asignacion_reposicion = "TODO";
	filtro_proceso_asignacion_reposicion = "TODO";
	filtro_ruta_asignacion_reposicion = "TODO";
}


function asignacion_reposicion_ingresar_cumple_filtros(data)
{
	var cumple = true;
	if(filtro_cuenta_asignacion_reposicion!= "" && filtro_cuenta_asignacion_reposicion != data.ncuenta)
	{
		console.log("no1");
		return false;
	}
	if(filtro_tarifa_asignacion_reposicion != "TODO" && filtro_tarifa_asignacion_reposicion != data.tarifa)
	{
		console.log("no2");
		return false;
	}
	if(filtro_motivo_asignacion_reposicion != "TODO" && filtro_motivo_asignacion_reposicion != data.motivo)
	{
		console.log("no3");
		return false;
	}
	if(filtro_proceso_asignacion_reposicion != "TODO" && filtro_proceso_asignacion_reposicion != data.id_proceso)
	{
		console.log("no4");
		return false;
	}
	if(filtro_proceso_asignacion_reposicion != "TODO" && filtro_ruta_asignacion_reposicion!="TODO" && filtro_ruta_asignacion_reposicion != data.id_ruta)
	{
		console.log("no5");
		return false;
	}
	var esta_asignado = (data.id_asignado != "" ? "A" : "SA"); 
	if(filtro_asignado_asignacion_reposicion != "TODO" && filtro_asignado_asignacion_reposicion != esta_asignado)
	{
		console.log("no6");
		return false;
	}
	return cumple;
}

// --------------------ANULACION-------------------------------

function inicia_asignacion_reposicion_anulacion(id_doc)
{
	asignacion_reposicion_doc_actual = id_doc;
	var datos = getJSON(api_general+"cbz_asignacion/datos_asignado",{codigo:asignacion_reposicion_doc_actual});

	$("#reposicion_anular_cabecera_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#reposicion_anular_contratista").val(datos.contratista);
	$("#reposicion_anular_movil").val(datos.movil);	

	var items = Array();
	$.each(datos.cuentas,function(i,val)
	{
		var item = {};
		item.n_cuenta = val[0];
		item.nom_cliente = val[1];
		item.tarifa = val[2];
		item.motivo = val[3];
		item.ruta = val[4];
		item.proceso = val[5];
		item.nom_estado = val[6];
		item.id_estado = val[7];
		item.id_registro = val[8];
		item.permite_cambio_estado = val[9];
		item.modificado = "S";

		item.hora = val[10];
		item.glosa = val[11];
		item.n_sello = val[12];
		item.fecha = val[13];


		items.push(item);
	});

	if(dt_estado_corte != undefined)
	{
		dt_estado_corte.destroy();
	}
	$("#reposicion_anular_cuenta_tabla> tbody tr").remove();
	$.each(items,function(i,val)
	{
		var id_detalle = val.id_registro;
		var row = "<tr>";
		row += "<td>"+val.n_cuenta+"</td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.nom_cliente+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.tarifa+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.motivo+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.ruta+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.proceso+"' readonly></td>";
		if(val.id_estado!="")
		{
			row += "<td><span class='label label-info'>"+val.nom_estado+" <i class='fas fa-history'></i></span></td>";
		}
		else
		{
			row += "<td><span class='label label-danger'>Sin registro <i class='fas fa-exclamation-triangle'></i></span></td>";
		}
		row += "</tr>";
		agregarFilaATabla("reposicion_anular_cuenta_tabla",row);
	});	


	dt_estado_corte = $('#reposicion_anular_cuenta_tabla').DataTable( 
    {
    	"paging": false,
    	"searching": false,
        "bSort": false,
    	"info": false,
        "oLanguage": es_leng,
        // "order" : [[0,"DESC"]],
    });
    $("#reposicion_anular_cuenta_tabla_wrapper .ui-corner-tr").html("<div class='datatable_title_custom'><label>Listado de Cuentas Asignadas para Reposición</label></div>");

    // $("#corte_ingresar_cuenta_tabla_filter").addClass("pull-left");	


}
$(document).on("click","#reposicion_anular_guardar",function()
{
	if($("#reposicion_anular_cabecera_form").validationEngine('validate'))
	{
		datos = {};
		if(asignacion_reposicion_doc_actual!=0)
		{
			datos.codigo = asignacion_reposicion_doc_actual;
		}
		datos.motivo = $("#reposicion_anular_motivo").val();
				
	    abrir_dialog_message("Anulando Datos...","loading",{});
		$.post
		(
			api_general+"cbz_asignacion/anulacion_asignacion",
			datos,
			function(data)
			{
				asignacion_reposicion_doc_actual = data;
	    		cerrar_dialog_message();
    			abrir_dialog_message("Datos Anulados Correctamente","aceptar");
	    		contenidoSubPrincipal($("#asignacion_reposicion_submenu_listar").data("url"));
				inicia_listar_reposiciones_asignaciones();

			}
			, "json"
		)
		.fail(function(response) {
	    	cerrar_dialog_message();
	    	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
		});
	}
});