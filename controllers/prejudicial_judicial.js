var var_cobranza_listar_prejudicial_dt = undefined;
var var_buscador_cuenta_dialog_tabla = undefined;
var var_cobranza_listar_prejudicial_dt = undefined;
var var_cobranza_prejudicial_archivos_dt = undefined;
var var_cobranza_prejudicial_actual = undefined;
var var_prejudicial_judicial = undefined;
var var_buscador_cuenta_legal_dialog_instancia = undefined;
var var_prejudicial_archivos_subir = Array();
var var_prejudicial_archivos_precargados = Array();
var var_sw_cerrar_cobranza = false;
var idprop = 0;


function inicia_listar_prejudiciales()
{

	var permiso_crear = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_prejudiciales_agregar'});
	if(permiso_crear)
	{
		$("#botonera_superior").append('<button type="button" id="cobranza_listar_prejudiciales_agregar" data-url="views/cobranza/ingresar_prejudicial.php" class="btn btn-xl btn-accion" ><i class="fas fa-plus"></i> Nueva Cobranza</button>');
	}	

	if(var_cobranza_listar_prejudicial_dt != undefined)
	{
		var_cobranza_listar_prejudicial_dt.destroy();
	}

	var permiso_finalizar = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_prejudicial_finalizar'});
	var permiso_registrar = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_prejudicial_adjuntar'});
	var permiso_escalar_judicial = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_prejudicial_avanzar_judicial'});
	var permiso_ver = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_prejudicial_ver'});
	
	var_cobranza_listar_prejudicial_dt = iniciar_datatable_basica("cobranza_listar_prejudiciales_tabla",api_general+"cbz_legal/sp",function(valor){
		var doc_cod = valor[0];
		var buttons = "";
		var estado = valor[5];
		if(valor[5] == "C") // C: Creado, F: Finalizado
		{
			valor[5] = "Creado";
			if(permiso_finalizar)
			{
				buttons += '<button title="Finalizar Cobranza" type="button" id="cobranza_listar_prejudicial_finalizar" value="'+doc_cod+'" class="btn btn-sm btn-warning"><i class="fa fa-folder" aria-hidden="true"></i></button>';
			}
			if(permiso_finalizar)
			{
				buttons += '<button title="Añadir Registro" type="button" id="cobranza_listar_prejudicial_adjuntar" value="'+doc_cod+'" class="btn btn-sm btn-success"><i class="fa fa-paperclip" aria-hidden="true"></i></button>';
			}
		}
		else
		{
			if(valor[5])
			{
				if(permiso_finalizar)
				{
					buttons += '<button title="Finalizar Cobranza" type="button" id="cobranza_listar_prejudicial_finalizar" value="'+doc_cod+'" class="btn btn-sm btn-warning"><i class="fa fa-folder" aria-hidden="true"></i></button>';
				}
				if(permiso_finalizar)
				{
					buttons += '<button title="Añadir Registro" type="button" id="cobranza_listar_prejudicial_adjuntar" value="'+doc_cod+'" class="btn btn-sm btn-success"><i class="fa fa-paperclip" aria-hidden="true"></i></button>';
				}
			}
			else
			{
				valor[5] = "Finalizado";
			}
		}
		if(valor[6]=="P") //P: Prejudicial, J: Judicial
		{
			valor[6] = "Prejudicial";
			if(estado!="F")
			{
				if(permiso_escalar_judicial)
				{
					buttons +='<button title="Transformar a Judicial" type="button" id="cobranza_listar_prejudicial_avanzar_judicial" value="'+doc_cod+'" class="btn btn-sm btn-info"><i class="fa fa-balance-scale" aria-hidden="true"></i></button>';
				}
			}
		}
		else
		{
			valor[6] = "Judicial";
		}
		if(permiso_ver)
		{
			buttons += '<button title="Ver Cobranza" type="button" id="cobranza_listar_prejudicial_ver" value="'+doc_cod+'" class="btn btn-sm btn-info"><i class="fa fa-eye" aria-hidden="true"></i></button>';
		}
		valor.push(buttons);
		return valor;
	},true);
}

$(document).on("click","#cobranza_listar_prejudiciales_agregar",function()
{
	inicia_ingresar_prejudicial();
});

function inicia_ingresar_prejudicial()
{
	var_prejudicial_judicial = "P";
	var_cobranza_prejudicial_actual = 0;
	var_prejudicial_archivos_subir = Array();
	var_prejudicial_archivos_precargados = Array();
	var_sw_cerrar_cobranza = false;
	idprop = 0;

	$("#cobranza_ingresar_prejudicial_cabecera_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#form_dialog_buscar_prejudicial_cuenta").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#cobranza_ingresar_prejudicial_archivo_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});

	// $("#cobranza_ingresar_prejudicial_cuenta_form").validationEngine({binded:true, autoHidePrompt:true, autoHideDelay:6500});

	contenidoSubPrincipal("views/cobranza/ingresar_prejudicial.php");
	if(var_cobranza_prejudicial_archivos_dt != undefined)
	{
		var_cobranza_prejudicial_archivos_dt.destroy();
	}
	var_cobranza_prejudicial_archivos_dt = iniciar_datatable_basica_vacia("cobranza_ingresar_prejudicial_cuenta_tabla",false);

	$("#cobranza_ingresar_prejudicial_cuenta_fecha").datepicker({defaultDate:new Date(), dateFormat: "dd/mm/yy"});
	$("#cobranza_ingresar_prejudicial_cuenta_fecha").datepicker( "setDate", new Date() );

	if(var_buscador_cuenta_legal_dialog_instancia != undefined)
	{
		var_buscador_cuenta_legal_dialog_instancia.dialog("destroy").remove();
	}

	var_buscador_cuenta_legal_dialog_instancia =  $("#dialog_buscador_prejudicial_cuenta").dialog({
		modal: true,
		buttons: {"CERRAR": function(){$(this).dialog("close");}},
		autoOpen: false,
		closeOnEscape: true,
		minWidth: 950,
		resizable: false,
		maxHeight:600,
	});
	if(var_buscador_cuenta_dialog_tabla != undefined)
	{
		var_buscador_cuenta_dialog_tabla.destroy();
	}
	var_buscador_cuenta_dialog_tabla = iniciar_datatable_basica_vacia("buscador_cuenta_prejudicial_resultados",false);
	$("#cobranza_ingresar_prejudicial_cuenta_numero").focus();
	$("#cobranza_ingresar_prejudicial_cuenta_numero").select();
}

$(document).on("click","#cobranza_listar_prejudicial_avanzar_judicial",function()
{
	var id_doc_actual = $(this).val();
	var buttons_dialog = [{text:"Cancelar", click:function(){$("#error_message_dialog").dialog('close');}},{text:"Procesar a Judicial", click:function(){traspasar_prejudicial_a_judicial(id_doc_actual);cerrar_dialog_message();}}];
	abrir_dialog_message("¿Está seguro de querer pasar la cobranza con ID "+id_doc_actual+" a cobranza Judicial?",'alerta',buttons_dialog);
});

function traspasar_prejudicial_a_judicial(iddoc)
{
	var datos = {};
	datos.leg_id = iddoc;
	$.post
	(
		api_general+"cbz_legal/judicial",
		datos,
		function(result)
		{
			abrir_dialog_message("Proceso Judicial Iniciado",'aceptar');
			inicia_listar_prejudiciales();
			cambia_submenu_actual("prejudiciales_submenu_listar")
		}
		, "json"
	);
}

$(document).on("click","#cobranza_listar_prejudicial_finalizar",function()
{
	inicia_ingresar_prejudicial();
	var_sw_cerrar_cobranza = true;
	cambia_submenu_actual("cobranza_submenu_ingresar_prejudicial")
	var_cobranza_prejudicial_actual = $(this).val();
	var datos = getJSON(api_general+"cbz_legal/get",{ld_id:$(this).val()});

	var_prejudicial_judicial = datos.tipo;
	$("#cobranza_ingresar_prejudicial_tipo_header").show();
	$("#cobranza_ingresar_prejudicial_tipo_header_edit").html((var_prejudicial_judicial=="P") ? "Cerrando: Proceso de Cobranza Prejudicial" : "Cerrando: Proceso de Cobranza Judicial");

	$("#cobranza_ingresar_prejudicial_cuenta_volver").show();

	var getCu = {};
	getCu.cuenta = datos.cta_nro_cuenta;
	var data = getJSON(api_general+"cuentas/get",getCu);
	$("#cobranza_ingresar_prejudicial_cuenta_numero").val(data.cuenta);
	$("#cobranza_ingresar_prejudicial_cuenta_rut").val(data.rut);
	$("#cobranza_ingresar_prejudicial_cuenta_nombre").val(data.nombre);
	$("#cobranza_ingresar_prejudicial_cuenta_direccion").val(data.direccion);
	$("#cobranza_ingresar_prejudicial_cuenta_tarifa").val(data.tarifa);

	$("#cobranza_ingresar_prejudicial_cuenta_numero").prop("disabled",true);
	$("#cobranza_ingresar_prejudicial_cuenta_buscar").hide();

	$("#cobranza_ingresar_prejudicial_cuenta_guardar").html('<i class="far fa-calendar-check"></i> Finalizar');

	for (var i = 0; i < datos.items.length; i++) {
		var fecItem = new Date(datos.items[i].fecha);
		datos.items[i].fechaMostrar = date_to_show(fecItem);
	}
	var_prejudicial_archivos_precargados = datos.items;
	actualizar_tabla_prejudicial_agregados();
	// console.log(cortes_data_cuentas_agregadas);
});



$(document).on("click","#cobranza_ingresar_prejudicial_cuenta_buscar",function()
{
	$("#buscador_cuenta_ncuenta").val("");
	$("#buscador_cuenta_rut").val("");
	$("#buscador_cuenta_nombre").val("");
	$("#buscador_cuenta_direccion").val("");
	$("#buscador_cuenta_prejudicial_resultados> tbody tr").remove();

	$("#dialog_buscador_prejudicial_cuenta").dialog("open");
});

$(document).on("click","#buscador_cuenta_prejudicial_filtrar",function()
{
	if($("#form_dialog_buscar_prejudicial_cuenta").validationEngine('validate'))
	{
		var data = {};
		!isEmpty($("#buscador_cuenta_ncuenta").val()) ? data.cuenta = $("#buscador_cuenta_ncuenta").val() : undefined;
		!isEmpty($("#buscador_cuenta_rut").val()) ? data.rut = $("#buscador_cuenta_rut").val() : undefined;
		!isEmpty($("#buscador_cuenta_nombre").val()) ? data.nombre = $("#buscador_cuenta_nombre").val() : undefined;
		!isEmpty($("#buscador_cuenta_direccion").val()) ? data.direccion = $("#buscador_cuenta_direccion").val() : undefined;

		data.sw_corte = "S";
		var datos = getJSON(api_general+"cuentas/sp_filtro",data);
		datos = datos.data;

		$("#buscador_cuenta_prejudicial_resultados> tbody tr").remove();
		for (var i = datos.length - 1; i >= 0; i--) {
			var row = "<tr role='row'>";
			row += "<td class='txtc'>"+datos[i][0]+"</td>";
			row += "<td class='txtc'>"+datos[i][1]+"</td>";
			row += "<td class='txtc'>"+datos[i][2]+"</td>";
			row += "<td class='txtc'>"+datos[i][3]+"</td>";
			row += "<td class='txtc'><button class='btn btn-sm btn-accion' value='"+datos[i][0]+"' id='buscador_cuenta_prejudicial_boton_cargar'>Cargar</button></td>";
			row += "</tr>";
			agregarFilaATabla("buscador_cuenta_prejudicial_resultados",row);
		}
	}
});

$(document).on("click","#buscador_cuenta_prejudicial_boton_cargar",function()
{
	ingresar_prejudicial_cargar_cuenta($(this).val());
});

$(document).on("blur","#cobranza_ingresar_prejudicial_cuenta_numero",function()
{
	if($(this).val()!="")
	{
		ingresar_prejudicial_cargar_cuenta($(this).val());
	}
	else
	{
		$("#cobranza_ingresar_prejudicial_cuenta_rut").val("");
		$("#cobranza_ingresar_prejudicial_cuenta_nombre").val("");
		$("#cobranza_ingresar_prejudicial_cuenta_direccion").val("");
		$("#cobranza_ingresar_prejudicial_cuenta_tarifa").val("");
	}
});

$(document).on("click","#cobranza_ingresar_prejudicial_cuenta_agregar",function()
{
	if($("#cobranza_ingresar_prejudicial_archivo_form").validationEngine("validate"))
	{
			var item = {};
			item.idprop = idprop++;
			var archivosInput = $("#cobranza_ingresar_prejudicial_cuenta_archivo")[0].files; 
			item.file = archivosInput[0];

			item.fechaMostrar = date_to_show($("#cobranza_ingresar_prejudicial_cuenta_fecha").datepicker("getDate"));
			item.fecha = date_to_php($("#cobranza_ingresar_prejudicial_cuenta_fecha").datepicker("getDate"));
			item.glosa = $("#cobranza_ingresar_prejudicial_cuenta_glosa").val();
			item.observacion = $("#cobranza_ingresar_prejudicial_cuenta_observacion").val();

			var_prejudicial_archivos_subir.push(item);
			actualizar_tabla_prejudicial_agregados();

		limpiar_campos_agregar_archivo_prejudicial();
		$("#cobranza_ingresar_prejudicial_cuenta_archivo").focus();
		$("#cobranza_ingresar_prejudicial_cuenta_archivo").select();
	}
});


$(document).on("click","#cobranza_ingresar_prejudicial_agregada_borrar",function()
{
	var newItems = Array();
	var eliminarArchivo = $(this).val();
	for (var i = 0; i < var_prejudicial_archivos_subir.length; i++) 
	{
		if(var_prejudicial_archivos_subir[i].idprop != eliminarArchivo)
		{
			newItems.push(var_prejudicial_archivos_subir[i]);
		}
	}
	var_prejudicial_archivos_subir = newItems;
	actualizar_tabla_prejudicial_agregados();
});

$(document).on("click","#cobranza_listar_prejudicial_ver",function()
{
	inicia_ingresar_prejudicial();
	cambia_submenu_actual("cobranza_submenu_ingresar_prejudicial")
	var_cobranza_prejudicial_actual = $(this).val();
	var datos = getJSON(api_general+"cbz_legal/get",{ld_id:$(this).val()});

	var_prejudicial_judicial = datos.tipo;
	$("#cobranza_ingresar_prejudicial_tipo_header").show();
	$("#cobranza_ingresar_prejudicial_tipo_header_edit").html((var_prejudicial_judicial=="P") ? "Proceso de Cobranza Prejudicial" : "Proceso de Cobranza Judicial");
	// if(datos.estado == "F")
	{
		$("#cobranza_ingresar_prejudicial_archivo_form").hide();
		$("#cobranza_ingresar_prejudicial_cuenta_guardar").hide();
	}
	$("#cobranza_ingresar_prejudicial_cuenta_volver").show();


	var getCu = {};
	getCu.cuenta = datos.cta_nro_cuenta;
	var data = getJSON(api_general+"cuentas/get",getCu);
	$("#cobranza_ingresar_prejudicial_cuenta_numero").val(data.cuenta);
	$("#cobranza_ingresar_prejudicial_cuenta_rut").val(data.rut);
	$("#cobranza_ingresar_prejudicial_cuenta_nombre").val(data.nombre);
	$("#cobranza_ingresar_prejudicial_cuenta_direccion").val(data.direccion);
	$("#cobranza_ingresar_prejudicial_cuenta_tarifa").val(data.tarifa);

	$("#cobranza_ingresar_prejudicial_cuenta_numero").prop("disabled",true);
	$("#cobranza_ingresar_prejudicial_cuenta_buscar").hide();


	for (var i = 0; i < datos.items.length; i++) {
		var fecItem = new Date(datos.items[i].fecha);
		datos.items[i].fechaMostrar = date_to_show(fecItem);
	}
	var_prejudicial_archivos_precargados = datos.items;
	actualizar_tabla_prejudicial_agregados();
	// console.log(cortes_data_cuentas_agregadas);
});

$(document).on("click","#cobranza_listar_prejudicial_adjuntar",function()
{
	inicia_ingresar_prejudicial();
	cambia_submenu_actual("cobranza_submenu_ingresar_prejudicial")
	var_cobranza_prejudicial_actual = $(this).val();
	var datos = getJSON(api_general+"cbz_legal/get",{ld_id:$(this).val()});

	var_prejudicial_judicial = datos.tipo;
	$("#cobranza_ingresar_prejudicial_tipo_header").show();
	$("#cobranza_ingresar_prejudicial_tipo_header_edit").html((var_prejudicial_judicial=="P") ? "Proceso de Cobranza Prejudicial" : "Proceso de Cobranza Judicial");
	$("#cobranza_ingresar_prejudicial_cuenta_volver").show();


	var getCu = {};
	getCu.cuenta = datos.cta_nro_cuenta;
	var data = getJSON(api_general+"cuentas/get",getCu);
	$("#cobranza_ingresar_prejudicial_cuenta_numero").val(data.cuenta);
	$("#cobranza_ingresar_prejudicial_cuenta_rut").val(data.rut);
	$("#cobranza_ingresar_prejudicial_cuenta_nombre").val(data.nombre);
	$("#cobranza_ingresar_prejudicial_cuenta_direccion").val(data.direccion);
	$("#cobranza_ingresar_prejudicial_cuenta_tarifa").val(data.tarifa);

	$("#cobranza_ingresar_prejudicial_cuenta_numero").prop("disabled",true);
	$("#cobranza_ingresar_prejudicial_cuenta_buscar").hide();


	for (var i = 0; i < datos.items.length; i++) {
		var fecItem = new Date(datos.items[i].fecha);
		datos.items[i].fechaMostrar = date_to_show(fecItem);
	}
	var_prejudicial_archivos_precargados = datos.items;
	actualizar_tabla_prejudicial_agregados();
	// console.log(cortes_data_cuentas_agregadas);
});

function actualizar_tabla_prejudicial_agregados()
{
	console.log(var_prejudicial_archivos_subir);
	$("#cobranza_ingresar_prejudicial_cuenta_tabla> tbody tr").remove();
	var tipo_archivo = "";
	for (var i = 0; i < var_prejudicial_archivos_precargados.length; i++)
	{
		var row = "<tr role='row'>";
		tipo_archivo = "Judicial";
		if(var_prejudicial_archivos_precargados[i].sw_legal=="P")
		{
			tipo_archivo = "Prejudicial";
		}
		row += "<td>"+tipo_archivo+"</td>";
		row += "<td>"+var_prejudicial_archivos_precargados[i].fechaMostrar+"</td>";
		row += "<td>"+var_prejudicial_archivos_precargados[i].glosa+"</td>";
		row += "<td>"+var_prejudicial_archivos_precargados[i].observacion+"</td>";
		if(isEmpty(var_prejudicial_archivos_precargados[i].nombre))
		{	
			row += "<td></td>";
		}	
		else
		{
			row += "<td><button class='btn btn-sm btn-accion' value='"+var_prejudicial_archivos_precargados[i].adj_id+"' id='cobranza_ingresar_prejudicial_agregada_descargar'><i class='fa fa-download' aria-hidden='true'></i></button></td>";
		}
		row += "</tr>";
		agregarFilaATabla("cobranza_ingresar_prejudicial_cuenta_tabla",row);
		
	}

	tipo_archivo = "Judicial";
	if(var_prejudicial_judicial=="P")
	{
		tipo_archivo = "Prejudicial";
	}

	for (var i = 0; i < var_prejudicial_archivos_subir.length; i++) 
	{
		var row = "<tr role='row'>";
		row += "<td>"+tipo_archivo+"</td>";
		row += "<td>"+var_prejudicial_archivos_subir[i].fechaMostrar+"</td>";
		row += "<td>"+var_prejudicial_archivos_subir[i].glosa+"</td>";
		row += "<td>"+var_prejudicial_archivos_subir[i].observacion+"</td>";
		row += "<td><button class='btn btn-sm btn-accion' value='"+var_prejudicial_archivos_subir[i].idprop+"' id='cobranza_ingresar_prejudicial_agregada_borrar'>Eliminar</button></td>";
		row += "</tr>";
		agregarFilaATabla("cobranza_ingresar_prejudicial_cuenta_tabla",row);
	}
}

function ingresar_prejudicial_cargar_cuenta(cta)
{
	var datos = {};
	datos.cuenta = cta;
	datos.sw_corte = "S";
	var data = getJSON(api_general+"cuentas/get",datos);

	$("#cobranza_ingresar_prejudicial_cuenta_numero").val(data.cuenta);
	$("#cobranza_ingresar_prejudicial_cuenta_rut").val(data.rut);
	$("#cobranza_ingresar_prejudicial_cuenta_nombre").val(data.nombre);
	$("#cobranza_ingresar_prejudicial_cuenta_direccion").val(data.direccion);
	$("#cobranza_ingresar_prejudicial_cuenta_tarifa").val(data.tarifa);

	$("#dialog_buscador_prejudicial_cuenta").dialog("close");
}

function limpiar_campos_agregar_archivo_prejudicial()
{

	$("#cobranza_ingresar_prejudicial_cuenta_archivo").val("");
	$("#cobranza_ingresar_prejudicial_cuenta_fecha").datepicker( "setDate", new Date() );
	$("#cobranza_ingresar_prejudicial_cuenta_glosa").val("");
	$("#cobranza_ingresar_prejudicial_cuenta_observacion").val("");

}

function date_to_php(d)
{

	var datestring = d.getFullYear() + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" + d.getDate()).slice(-2);
    return datestring;
}


function date_to_show(d)
{
	
	var datestring = ("0" + d.getDate()).slice(-2) + "/" + ("0"+(d.getMonth()+1)).slice(-2) + "/" + d.getFullYear();
    return datestring;
}

$(document).on("click","#cobranza_ingresar_prejudicial_cuenta_guardar",function()
{
	ingresar_prejudicial_guardar_datos(true);
});

function ingresar_prejudicial_guardar_datos(cerrar)
{
	if(var_cobranza_prejudicial_actual!=0 && var_sw_cerrar_cobranza)
	{

		var formData = new FormData();
	    formData.append('leg_id', var_cobranza_prejudicial_actual);

		for (var i = 0; i < var_prejudicial_archivos_subir.length; i++)
		{
			if(var_prejudicial_archivos_subir[i].file== undefined)
			{
				var_prejudicial_archivos_subir[i].sw_file = "N";
			}
			else
			{
	    		formData.append('file'+i, var_prejudicial_archivos_subir[i].file);
				var_prejudicial_archivos_subir[i].sw_file = "S";
			}
			var_prejudicial_archivos_subir[i].sw_legal = var_prejudicial_judicial;
		}
	    formData.append('items', JSON.stringify(var_prejudicial_archivos_subir));
	    var xhr = new XMLHttpRequest();
	    xhr.open('POST', api_general+"cbz_legal/finalizar", true);
	    xhr.onreadystatechange = function (oEvent) {  
		    if (xhr.readyState === 4) {  
		        if (xhr.status === 200) {  
		        	cerrar_dialog_message();
		    		abrir_dialog_message("Cobranza Judicial Finalizada","aceptar");
					contenidoSubPrincipal($("#prejudiciales_submenu_listar").data("url"));
					inicia_listar_prejudiciales();
					cambia_submenu_actual("prejudiciales_submenu_listar");
		        } else {  
		        	cerrar_dialog_message();
		        	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
		        }  
		    }  
		}; 

	    abrir_dialog_message("Guardando Datos...","loading",{});
	  //   xhr.addEventListener("load", function(){
			// abrir_dialog_message("Cobro Iniciado",'aceptar');
			// inicia_listar_prejudiciales();
			// cambia_submenu_actual("prejudiciales_submenu_listar")
	  //   }, false);
	    xhr.send(formData);
	}
	else
	{		
		if($("#cobranza_ingresar_prejudicial_cabecera_form").validationEngine("validate"))
		{
			
			var formData = new FormData();
		    formData.append('cuenta', $("#cobranza_ingresar_prejudicial_cuenta_numero").val());
		    if(var_cobranza_prejudicial_actual!=0)
		    {
		    	formData.append('leg_id', var_cobranza_prejudicial_actual);
		    }

			for (var i = 0; i < var_prejudicial_archivos_subir.length; i++)
			{
				if(var_prejudicial_archivos_subir[i].file== undefined)
				{
					var_prejudicial_archivos_subir[i].sw_file = "N";
				}
				else
				{
		    		formData.append('file'+i, var_prejudicial_archivos_subir[i].file);
					var_prejudicial_archivos_subir[i].sw_file = "S";
				}
				var_prejudicial_archivos_subir[i].sw_legal = var_prejudicial_judicial;
			}
		    formData.append('items', JSON.stringify(var_prejudicial_archivos_subir));
		    var xhr = new XMLHttpRequest();
		    xhr.open('POST', api_general+"cbz_legal/cu", true);
		    xhr.onreadystatechange = function (oEvent) {  
			    if (xhr.readyState === 4) {  
			        if (xhr.status === 200) {  
			        	cerrar_dialog_message();
			    		abrir_dialog_message("Cobranza Judicial Finalizada","aceptar");
						contenidoSubPrincipal($("#prejudiciales_submenu_listar").data("url"));
						inicia_listar_prejudiciales();
						cambia_submenu_actual("prejudiciales_submenu_listar");
			        } else {  
			        	cerrar_dialog_message();
			        	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
			        }  
			    }  
			}; 

		    abrir_dialog_message("Guardando Datos...","loading",{});
		    xhr.send(formData);
		}
	}
}

$(document).on("click","#cobranza_ingresar_prejudicial_cuenta_volver",function()
{
	contenidoSubPrincipal($("#prejudiciales_submenu_listar").data("url"));
	cambia_submenu_actual("prejudiciales_submenu_listar");
	inicia_listar_prejudiciales();
});

// function prejudicial_adjuntar_archivos(idactual)
// {
// 	var formData = new FormData();
// 	for (var i = 0; i < var_prejudicial_archivos_subir.length; i++)
// 	{
// 		if(var_prejudicial_archivos_subir[i].file== undefined)
// 		{
// 			var_prejudicial_archivos_subir[i].sw_file = "N";
// 		}
// 		else
// 		{
//     		formData.append('file'+i, var_prejudicial_archivos_subir[i].file);
// 			var_prejudicial_archivos_subir[i].sw_file = "S";
// 		}
// 		var_prejudicial_archivos_subir[i].sw_legal = var_prejudicial_judicial;
// 	}
//     formData.append('id', idactual);
//     formData.append('items', JSON.stringify(var_prejudicial_archivos_subir));
//     var xhr = new XMLHttpRequest();
//     xhr.open('POST', 'api/legal_archivo/adjuntar', true);
//     xhr.addEventListener("load", function(){
// 		abrir_dialog_message("Cobro Iniciado",'aceptar');
// 		contenidoSubPrincipal($("#prejudiciales_submenu_listar").data("url"));
// 		inicia_listar_prejudiciales();
// 		cambia_submenu_actual("prejudiciales_submenu_listar");
//     }, false);
//     xhr.send(formData);
// }


$(document).on("click", "#cobranza_ingresar_prejudicial_agregada_descargar",function()
{
	var id = $(this).val();
	var url = api_general+"archivo/get/?"+id;
	window.open(url);
});