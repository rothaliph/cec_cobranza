var dt_corte = undefined;
var dt_estado_corte = undefined;
var corte_doc_actual = 0;
var corte_cuentas_datos = Array();
var corte_registro_actual = 0;
var estado_por_defecto_corte = 0;


function inicia_listar_cortes()
{
	// if(dt_corte != undefined)
	// {
	// 	dt_corte.destroy();
	// }
	// // var permiso_edit = getJSON(api_general+"usuarios/get_permiso",{id_ui:'corte_listar_editar'});
	// dt_corte = iniciar_datatable_basica("corte_listar_tabla",api_general+"cbz_corte/sp",function(valor){
	// 	var id_corte = valor[3];

	// 	var btnes = "";
	// 	// if(permiso_edit)
	// 	{
	// 		btnes += "<button type='button' class='btn btn-xs btn-accion' id='corte_listar_edit' title='Editar' data-url='views/cobranza/ingresar_corte.php' value='"+id_corte+"'><i class='far fa-edit'></i></button>";;
	// 	}

	// 	valor[3] =btnes;
	// 	return valor;
	// },undefined,undefined,true,columnDefs);
	// // var permiso_new = getJSON(api_general+"usuarios/get_permiso",{id_ui:'corte_listar_agregar'});
	// // if(permiso_new)
	// {
		// $("#botonera_superior").append('<button type="button" id="corte_listar_agregar" data-url="views/cobranza/ingresar_corte.php" class="btn btn-xl btn-accion" ><i class="fas fa-plus"></i> Registrar Cortes</button>');
	// }	
}

// $(document).on("click","#corte_listar_agregar",function()
// {
// 	contenidoSubPrincipal($(this).data("url"));
// 	inicia_corte();
// });

function inicia_corte(id_asignacion)
{
	corte_doc_actual = id_asignacion;
	$("#corte_ingresar_cabecera_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#corte_ingresar_cabecera_cuenta_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#corte_ingresar_cuenta_hora").datetimepicker({format:"H:i", datepicker:false, mask:true, defaultTime: "now"});
	$("#corte_ingresar_cuenta_fecha").datetimepicker({format:"d/m/Y", timepicker:false, mask:true, defaultDate:new Date()});
	
	corte_cuentas_datos = Array();
	corte_registro_actual = 0;
	corte_ingresar_registro_deshabilitar_inputs();
	var estados_de_corte = getJSON(api_general+"cbz_asignacion_estado/s2",{tipo:"C"});
	$("#corte_ingresar_cuenta_estado").select2({ data: estados_de_corte });
	estado_por_defecto_corte = "";
	$.each(estados_de_corte,function(i,estado)
	{
		if(estado.data.finaliza=="S")
		{
			estado_por_defecto_corte = estado.id;
		}
	});

	var datos = getJSON(api_general+"cbz_asignacion/datos_asignado",{codigo:id_asignacion});
	
	$("#corte_ingresar_contratista").val(datos.contratista);
	$("#corte_ingresar_movil").val(datos.movil);

	$.each(datos.cuentas,function(i,val)
	{
		var item = {};
		item.n_cuenta = val[0];
		item.nom_cliente = val[1];
		item.tarifa = val[2];
		item.motivo = val[3];
		item.ruta = val[4];
		item.proceso = val[5];
		item.nom_estado = val[6];
		item.id_estado = val[7];
		item.id_registro = val[8];
		item.permite_cambio_estado = val[9];
		item.modificado = "S";

		item.hora = val[10];
		item.glosa = val[11];
		item.n_sello = val[12];
		item.fecha = val[13];


		corte_cuentas_datos.push(item);
	});
	corte_ingresar_actualiza_datos_tabla();
	$("#corte_ingresar_cuenta_numero").select();
}

$(document).on("change",".corte_ingresar_check_asignar",function()
{
	if($(this).data("asignado")!='')
	{
		if($(this).prop('checked'))
		{		
			id_check_actual_revisado = $(this).val();
			abrir_dialog_message("Está reasignando una cuenta<br> ¿está seguro?","alerta",[{text:"Aceptar", click:corte_ingresar_check_asignar_aceptar},{text:"Cancelar", click:corte_ingresar_check_asignar_cancelar}]);
		}
	}
});

function corte_ingresar_actualiza_datos_tabla()
{
	if(dt_estado_corte != undefined)
	{
		dt_estado_corte.destroy();
	}
	$("#corte_ingresar_cuenta_tabla> tbody tr").remove();
	$.each(corte_cuentas_datos,function(i,val)
	{
		var id_detalle = val.id_registro;
		var row = "<tr>";
		row += "<td>"+val.n_cuenta+"</td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.nom_cliente+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.tarifa+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.motivo+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.ruta+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.proceso+"' readonly></td>";
		if(val.id_estado!="")
		{
			row += "<td><span class='label label-info'>"+val.nom_estado+" <i class='fas fa-history'></i></span></td>";
		}
		else
		{
			row += "<td><span class='label label-danger'>Sin registro <i class='fas fa-exclamation-triangle'></i></span></td>";
		}
	
		row += "<td>";
		if(val.permite_cambio_estado=="S")
		{
			row += "	<button type='button' id='corte_listar_item_cargar' value='"+val.id_registro+"' class='btn btn-sm btn-success' ><i class='fas fa-plus'></i></button>";
		}
		row += "</td>";
		row += "</tr>";
		agregarFilaATabla("corte_ingresar_cuenta_tabla",row);
	});	
	// dt_estado_corte = iniciar_datatable_basica_vacia("corte_ingresar_cuenta_tabla",true);

	dt_estado_corte = $('#corte_ingresar_cuenta_tabla').DataTable( 
    {
    	"paging": false,
    	"searching": true,
        "bSort": false,
    	"info": false,
        "oLanguage": es_leng,
        // "order" : [[0,"DESC"]],
    });
    $("#corte_ingresar_cuenta_tabla_wrapper .ui-corner-tr").html("<div class='datatable_title_custom'><label>Listado de Estados de Cuentas para Corte</label></div>");

    $("#corte_ingresar_cuenta_tabla_filter").addClass("pull-left");

}

$(document).on("click","#corte_listar_item_cargar",function()
{
	corte_ingresar_cargar_registro($(this).val());
});

$(document).on("click","#corte_ingresar_guardar",function()
{
	corte_ingresar_guardar_datos(false);
});

$(document).on("click","#corte_ingresar_guardar_continuar",function()
{
	corte_ingresar_guardar_datos(true);
});

$(document).on("click","#corte_ingresar_volver",function()
{
	corte_ingresar_volver();
	// abrir_dialog_message("Volverá a la lista de asignaciones y perderá los datos que no hayan sido guardados.<br> ¿Está seguro?","alerta",[{text:"Aceptar", click:corte_ingresar_volver},{text:"Cancelar", click:cerrar_dialog_message}]);
});

function corte_ingresar_volver()
{
	contenidoSubPrincipal($("#asignacion_submenu_listar").data("url"));
	inicia_listar_asignaciones();
	cerrar_dialog_message();
}


function corte_ingresar_guardar_datos(continuar)
{
	// if($("#corte_ingresar_cabecera_form").validationEngine('validate'))
	{
		datos = {};
		if(corte_doc_actual!=0)
		{
			datos.id = corte_doc_actual;
		}

		var items = Array();
		$.each(corte_cuentas_datos,function(i,val)
		{
			if(val.modificado=="S")
			{			
				var item = {};

				item.id = val.id_registro;
				item.fecha = val.fecha;
				item.hora = val.hora;
				item.estado = val.id_estado;
				item.sello = val.n_sello;
				item.observacion = val.glosa;
				items.push(item);
			}
		});
		datos.items = items;


	    abrir_dialog_message("Guardando Datos...","loading",{});
		$.post
		(
			api_general+"cbz_asignacion/cambio_estado",
			datos,
			function(data)
			{
				corte_doc_actual = data;
	    		cerrar_dialog_message();
	    		abrir_dialog_message("Datos Guardados Correctamente","aceptar");

	    		if(!continuar)
	    		{    			
	    			contenidoSubPrincipal($("#asignacion_submenu_listar").data("url"));
					inicia_listar_asignaciones();
					cerrar_dialog_message();
	    		}
	    		else
	    		{
	    			contenidoSubPrincipal("views/cobranza/ingresar_corte.php");
	    			inicia_corte(corte_doc_actual);
	    		}
			}
			, "json"
		)
		.fail(function(response) {
	    	cerrar_dialog_message();
	    	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
		});
	}
}

function corte_ingresar_registro_deshabilitar_inputs()
{
	$("#corte_ingresar_cuenta_editar_aceptar").prop("disabled",true);
	$("#corte_ingresar_cuenta_editar_cancelar").prop("disabled",true);
	$("#corte_ingresar_cuenta_estado").prop("disabled",true);
	$("#corte_ingresar_cuenta_observacion").prop("disabled",true);
	$("#corte_ingresar_cuenta_fecha").prop("disabled",true);
	$("#corte_ingresar_cuenta_hora").prop("disabled",true);
	$("#corte_ingresar_cuenta_sello").prop("disabled",true);

	$("#corte_ingresar_cuenta_estado").select2("val","");
	$("#corte_ingresar_cuenta_observacion").val("");
	$("#corte_ingresar_cuenta_fecha").val("");
	$("#corte_ingresar_cuenta_hora").val("");
	$("#corte_ingresar_cuenta_sello").val("");
	$("#corte_ingresar_cuenta_numero").val("");
	$("#corte_ingresar_cuenta_rut").val("");
	$("#corte_ingresar_cuenta_nombre").val("");
	$("#corte_ingresar_cuenta_direccion").val("");
	$("#corte_ingresar_cuenta_tarifa").val("");		
}
function corte_ingresar_registro_habilitar_inputs()
{
	$("#corte_ingresar_cuenta_editar_aceptar").prop("disabled",false);
	$("#corte_ingresar_cuenta_editar_cancelar").prop("disabled",false);
	$("#corte_ingresar_cuenta_estado").prop("disabled",false);
	$("#corte_ingresar_cuenta_observacion").prop("disabled",false);
	$("#corte_ingresar_cuenta_fecha").prop("disabled",false);
	$("#corte_ingresar_cuenta_hora").prop("disabled",false);
	$("#corte_ingresar_cuenta_sello").prop("disabled",false);
}

$(document).on("click","#corte_ingresar_cuenta_editar_cancelar",function()
{
	corte_ingresar_registro_deshabilitar_inputs();
});

$(document).on("click","#corte_ingresar_cuenta_editar_aceptar",function()
{
	if($("#corte_ingresar_cabecera_cuenta_form").validationEngine('validate'))
	{
		var registro = {};
		var nuevos_datos = Array();
		$.each(corte_cuentas_datos,function(i,val)
		{
			if(val.id_registro == corte_registro_actual)
			{
				val.id_estado = $("#corte_ingresar_cuenta_estado").val();
				val.nom_estado = $("#corte_ingresar_cuenta_estado").select2("data").text;
				var finaliza_cuenta = $("#corte_ingresar_cuenta_estado").select2("data").data.finaliza;
				// val.permite_cambio_estado = finaliza_cuenta=="S"? "N": "S"; //si finaliza, no permite cambio estado, y vice versa
				val.fecha = $("#corte_ingresar_cuenta_fecha").val(); // MODIFICAR PARA GUARDAR FECHA ACTUAL
				val.hora = $("#corte_ingresar_cuenta_hora").val();
				val.n_sello = $("#corte_ingresar_cuenta_sello").val();
				val.glosa = $("#corte_ingresar_cuenta_observacion").val();
				val.modificado = "S";
			}
			nuevos_datos.push(val);
		});
		corte_cuentas_datos = nuevos_datos;

		corte_ingresar_actualiza_datos_tabla();
		corte_ingresar_registro_deshabilitar_inputs();
		$("#corte_ingresar_cuenta_estado").select2("val","");
		$("#corte_ingresar_cuenta_observacion").val("");
		$("#corte_ingresar_cuenta_fecha").val("");
		$("#corte_ingresar_cuenta_hora").val("");
		$("#corte_ingresar_cuenta_sello").val("");

		$("#corte_ingresar_cuenta_numero").focus();

		corte_ingresar_guardar_datos(true);
	}
});

function corte_ingresar_cargar_registro(id_registro)
{
	corte_registro_actual = id_registro;
	var registro = {};
	$.each(corte_cuentas_datos,function(i,val)
	{
		if(val.id_registro == id_registro)
		{
			registro = val;
		}
	});

	if(registro.id_estado!="")
	{
		$("#corte_ingresar_cuenta_estado").select2("val",registro.id_estado);
	}
	else
	{
		$("#corte_ingresar_cuenta_estado").select2("val",estado_por_defecto_corte); // CORTE POR DEFECTO
	}
	if(registro.hora!="")
	{
		$("#corte_ingresar_cuenta_hora").val(registro.hora);
	}
	else
	{
		$("#corte_ingresar_cuenta_hora").datetimepicker({value:new Date()});
	}
	if(registro.fecha!="")
	{
		$("#corte_ingresar_cuenta_fecha").val(registro.fecha);
	}
	else
	{
		$("#corte_ingresar_cuenta_fecha").datetimepicker({value:new Date()});
	}
	$("#corte_ingresar_cuenta_sello").val(registro.n_sello);
	$("#corte_ingresar_cuenta_observacion").val(registro.glosa);

	var datos = {};
	datos.cuenta = registro.n_cuenta;
	var data = getJSON(api_general+"cuentas/get",datos);

	$("#corte_ingresar_cuenta_numero").val(data.cuenta);
	$("#corte_ingresar_cuenta_rut").val(data.rut);
	$("#corte_ingresar_cuenta_nombre").val(data.nombre);
	$("#corte_ingresar_cuenta_direccion").val(data.direccion);
	$("#corte_ingresar_cuenta_tarifa").val(data.tarifa);

	corte_ingresar_registro_habilitar_inputs();
	$("#corte_ingresar_cuenta_estado").select2("open");
	$("#corte_ingresar_cuenta_estado").focus();
	$("#corte_ingresar_cuenta_estado").select();
}

// -----------------

$(document).on("blur","#corte_ingresar_cuenta_numero",function(e)
{
	e.stopPropagation();
	if($(this).val()!="" && $("#corte_ingresar_cabecera_form").validationEngine('validate'))
	{
		var cuenta = $("#corte_ingresar_cuenta_numero").val();
		var registro = 0;
		$.each(corte_cuentas_datos,function(i,val)
		{
			if(val.n_cuenta == cuenta && val.permite_cambio_estado == "S")
			{
				registro = val.id_registro;
			}
		});

		if(registro!=0)
		{
			corte_ingresar_cargar_registro(registro);			
		}
		else
		{
			abrir_dialog_message("La cuenta no puede ser cargada debida a que no se encuentra en la lista, o ya tiene un estado que no puede ser modificado","alerta");
			corte_ingresar_registro_deshabilitar_inputs();
			$("#corte_ingresar_cuenta_numero").select();
		}
	}
});