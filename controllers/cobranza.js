var dt_cobranza_masiva = undefined;
var dt_cobranza_individual = undefined;

function inicia_modulo_cobranza()
{
	pr_funcion = "funcion_vacia";
	pr_url = "/#";
	cargada = false; 
	var data = getJSON(api_general+"usuarios/get_permisos",{});
	$.each(data.pag,function(i,val)
	{
		if(!cargada)
		{
			pr_funcion = val.fn_js;
			pr_url = val.url;
			cargada = true;
		}
		var li = "<li role='presentation'><a id='"+val.nombre_ui+"' data-fn='"+val.fn_js+"' class='submenu-option' href='#' data-url='"+val.url+"'>"+val.nombre+"</a></li>";
		$("#submenu_cobranza").append(li);
	});
	
	contenidoSubPrincipal(pr_url);
	eval(pr_funcion).call();  
	
}
function inicia_listar_cobranza()
{
	var columnDefs = [
	{
	    "targets": [3],
	    "className": "txtr",
	    // "width": "4%"
	}];

	$("#div_cobranza_listar_tabla_individuales").hide();
	if(dt_cobranza_masiva != undefined)
	{
		dt_cobranza_masiva.destroy();
	}
	var permiso_ver = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_ver'});
	dt_cobranza_masiva = iniciar_datatable_basica("cobranza_listar_tabla_masiva",api_general+"cbz_proceso/sp_masivo",function(valor){
		var id_cobranza = valor[7];
		if(valor[1]=="D")
		{
			valor[1]= "Deuda";
		}
		else
		{
			if(valor[1]=="T")
			{
				valor[1]= "TE1 Vencido";
			}
			else
			{
				valor[1] = "Deuda y TE1 Venc."
			}
		}

		if(valor[2]=="B")
		{
			valor[2]= "Baja";
		}
		else
		{
			if(valor[2]=="M")
			{
				valor[2]= "Alta";
			}
			else
			{
				valor[2] = "Baja Y Alta"
			}
		}
		valor[3] = a_precio(valor[3],"$0,0");
		var btnes = "";
		if(permiso_ver)
		{
			btnes += "<button type='button' class='btn btn-xs btn-accion' id='cobranza_listar_ver' title='Ver' data-url='views/cobranza/generar_cobranza.php' value='"+id_cobranza+"'><i class='far fa-file-pdf'></i></button>";
			// btnes += "<button type='button' class='btn btn-xs btn-accion' id='cobranza_listar_ver_excel' title='Ver en Excel' data-url='views/cobranza/generar_cobranza.php' value='"+id_cobranza+"'><i class='far fa-file-excel'></i></button>";
		}

		valor[7] =btnes;
		return valor;
	},undefined,undefined,true,columnDefs);
	$("#cobranza_listar_tabla_masiva_wrapper .ui-corner-tr").append("<div class='datatable_title_custom'><label>Listado de Generaciones de Datos para Listado de Corte</label></div>");

	var permiso_new = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_agregar'});
	if(permiso_new)
	{
		$("#botonera_superior").append('<button type="button" id="cobranza_listar_agregar" data-url="views/cobranza/generar_cobranza.php" class="btn btn-xl btn-accion" ><i class="fas fa-plus"></i> Generar Listado de Corte</button>');
	}

	var permiso_deuda = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_deuda'});
	if(permiso_deuda)
	{
		$("#botonera_superior").append('<button type="button" id="cobranza_listar_deuda" data-url="views/cobranza/generar_cobranza.php" class="btn btn-xl btn-accion" ><i class="far fa-file-excel"></i> Generar Listado de Deuda</button>');
	}
	
}

$(document).on("change","#cobranza_listar_select",function()
{
	if($(this).val()=="1")
	{
		$("#div_cobranza_listar_tabla_masiva").show();
		$("#div_cobranza_listar_tabla_individuales").hide();
		var columnDefs = [
		{
		    "targets": [2],
		    "className": "txtr",
		}];
		if(dt_cobranza_masiva != undefined)
		{
			dt_cobranza_masiva.destroy();
		}
		var permiso_ver = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_ver'});
		dt_cobranza_masiva = iniciar_datatable_basica("cobranza_listar_tabla_masiva",api_general+"cbz_proceso/sp_masivo",function(valor){
			var id_cobranza = valor[7];
			if(valor[1]=="D")
			{
				valor[1]= "Deuda";
			}
			else
			{
				if(valor[1]=="T")
				{
					valor[1]= "TE1 Vencido";
				}
				else
				{
					valor[1] = "Deuda y TE1 Venc."
				}
			}

			if(valor[2]=="B")
			{
				valor[2]= "Baja";
			}
			else
			{
				if(valor[2]=="M")
				{
					valor[2]= "Alta";
				}
				else
				{
					valor[2] = "Baja Y Alta"
				}
			}
			valor[3] = a_precio(valor[3]);
			var btnes = "";
			if(permiso_ver)
			{
				btnes += "<button type='button' class='btn btn-xs btn-accion' id='cobranza_listar_ver' title='Ver' data-url='views/cobranza/generar_cobranza.php' value='"+id_cobranza+"'><i class='far fa-file-pdf'></i></button>";
				// btnes += "<button type='button' class='btn btn-xs btn-accion' id='cobranza_listar_ver_excel' title='Ver en Excel' data-url='views/cobranza/generar_cobranza.php' value='"+id_cobranza+"'><i class='far fa-file-excel'></i></button>";
			}
			valor[7] = btnes;
			return valor;
		},undefined,undefined,true,columnDefs);		
		$("#cobranza_listar_tabla_masiva_wrapper .ui-corner-tr").append("<div class='datatable_title_custom'><label>Listado de Generaciones de Datos para Cobranza</label></div>");
	}
	else
	{
		$("#div_cobranza_listar_tabla_masiva").hide();
		$("#div_cobranza_listar_tabla_individuales").show();
		var columnDefs = [
		{
		    "targets": [2],
		    "className": "txtr",
		}];
		if(dt_cobranza_individual != undefined)
		{
			dt_cobranza_individual.destroy();
		}
		var permiso_ver = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_ver'});
		var permiso_adjunto = getJSON(api_general+"usuarios/get_permiso",{id_ui:'cobranza_listar_adjunto'});
		dt_cobranza_individual = iniciar_datatable_basica("cobranza_listar_tabla_individuales",api_general+"cbz_proceso/sp_individual",function(valor){
			var id_cobranza = valor[5];
			var id_adjunto = valor[4];
			var item = Array();
			item.push(valor[0]);
			item.push(valor[1]);
			if(valor[2]=="D")
			{
				item.push("Deuda");
			}
			else
			{
				item.push("TE1 Vencido");
			
			}
			item.push(valor[3]);
			// valor[3] = a_precio(valor[3]);
			var btnes = "";
			if(permiso_adjunto)
			{
				if(!isEmpty(id_adjunto))
				{
						btnes += "<button type='button' class='btn btn-xs btn-accion' id='cobranza_listar_adjunto' title='Descargar Adjunto' value='"+id_adjunto+"'><i class='fas fa-paperclip'></i></button>";;
				}
			}
			
			if(permiso_ver)
			{
				btnes += "<button type='button' class='btn btn-xs btn-accion' id='cobranza_listar_ver' title='Ver' data-url='views/cobranza/generar_cobranza.php' value='"+id_cobranza+"'><i class='far fa-file-pdf'></i></button>";;
				// btnes += "<button type='button' class='btn btn-xs btn-accion' id='cobranza_listar_ver_excel' title='Ver en Excel' data-url='views/cobranza/generar_cobranza.php' value='"+id_cobranza+"'><i class='far fa-file-excel'></i></button>";;
			}

			item.push(btnes);
			return item;
		},undefined,undefined,true,columnDefs);	
		$("#div_cobranza_listar_tabla_individuales_wrapper .ui-corner-tr").append("<div class='datatable_title_custom'><label>Listado de Generaciones de Datos para Cobranza</label></div>");		
	}

});

function inicia_cobranza()
{
	$("#cobranza_tipo_masivo").hide();
	$("#cobranza_tipo_individual").hide();
	select2_comunas = getJSON(api_general+"comuna/s2");
	if(select2_comunas==undefined)
	{
		select2_comunas = Array();
	}
	select2_comunas.push({id:"T",text:"TODAS",data:{}});
	$("#cobranza_informe_comuna").select2({ data: select2_comunas });

	var select2_procesos = getJSON(api_general+"proceso/s2");
	if(select2_procesos==undefined)
	{
		select2_procesos = Array();
	}
	select2_procesos.push({id:"T",text:"TODOS",data:{}});
	$("#cobranza_informe_proceso").select2({ data: select2_procesos });


	$("#cobranza_informe_ruta").select2({ data: getJSON(api_general+"ruta/s2",{"proceso":0})});
	
	$("#cobranza_informe_proceso").on("change",function(e)
	{
		var id_proceso = $("#cobranza_informe_proceso").val();
		var new_data = Array();
		if(id_proceso!="T")
		{		
			new_data = getJSON(api_general+"ruta/s2",{"proceso":id_proceso});
			new_data.push({id:"T",text:"TODAS",data:{}});
			$("#cobranza_informe_ruta").prop('disabled',false);
			$("#cobranza_informe_ruta").select2({ data: new_data});	
			$("#cobranza_informe_ruta").select2('val','T');
		}
		else
		{
			new_data.push({id:"T",text:"TODAS",data:{}});
			$("#cobranza_informe_ruta").select2({ data: new_data});	
			$("#cobranza_informe_ruta").select2('val','T');
			$("#cobranza_informe_ruta").prop('disabled',true);

		}
	});


	// var select2_procesos = getJSON(api_general+"proceso/s2");

	$("#cobranza_informe_form_cabecera").validationEngine({autoHidePrompt:true, autoHideDelay:2500});
}

$(document).on("click","#cobranza_listar_agregar",function()
{
	contenidoSubPrincipal($(this).data("url"));
	inicia_cobranza();
});

$(document).on("blur","#cobranza_n_servicio",function()
{
	if($(this).val()!="")
	{
		var datos = {};
		datos.cuenta =  $(this).val();
			
		var data = getJSON(api_general+"cuentas/get",datos);
		if(data.length != 0)
		{
			$("#cobranza_nombre_cliente").val(data.nombre);
			$("#cobranza_direccion_cliente").val(data.direccion);
		}
		else
		{
			$("#cobranza_n_servicio").val("");
			$("#cobranza_nombre_cliente").val("");
			$("#cobranza_direccion_cliente").val("");
			$("#cobranza_n_servicio").focus();
			$("#cobranza_n_servicio").select();
			
		}
	}
	else
	{
		$("#cobranza_nombre_cliente").val("");
		$("#cobranza_direccion_cliente").val("");
	}
});
$(document).on("change","#cobranza_tipo_corte",function()
{
	var valor = $(this).val();
	if(valor =="")
	{
		$("#cobranza_informe_form_masivo").validationEngine('detach');
		$("#cobranza_informe_form_individual").validationEngine('detach');

		$("#cobranza_tipo_masivo").hide();
		$("#cobranza_tipo_individual").hide();
	}
	if(valor ==1)
	{
		$("#cobranza_informe_form_masivo").validationEngine({autoHidePrompt:true, autoHideDelay:2500});
		$("#cobranza_informe_form_individual").validationEngine('detach');
		
		$("#cobranza_tipo_masivo").show();
		$("#cobranza_tipo_individual").hide();
	}
	if(valor ==2)
	{
		$("#cobranza_informe_form_individual").validationEngine({autoHidePrompt:true, autoHideDelay:2500});
		$("#cobranza_informe_form_masivo").validationEngine('detach');

		$("#cobranza_tipo_masivo").hide();
		$("#cobranza_tipo_individual").show();
	}
});


$(document).on("click","#cobranza_listar_adjunto",function()
{
	window.open(api_general	+ "archivo/get/?"+$(this).val());
});

$(document).on("click","#cobranza_listar_ver",function()
{
	window.open("documentos/cobranza.php?codigo="+$(this).val());
});

$(document).on("click","#cobranza_listar_deuda",function()
{
	window.open("documentos/deuda.php");
});

$(document).on("click","#cobranza_generar_ot",function()
{
	if($("#cobranza_informe_form_cabecera").validationEngine("validate"))
	{
		datos = {};
		if($("#cobranza_tipo_corte").val()==1)
		{
			if($("#cobranza_informe_form_masivo").validationEngine("validate"))
			{
				datos.tipo = "masivo";
				datos.deuda = $("#cobranza_informe_motivo_deuda").is(":checked");
				datos.t1vencido = $("#cobranza_informe_motivo_t1").is(":checked");
				datos.bt = $("#cobranza_informe_tension_BT").is(":checked");
				datos.at = $("#cobranza_informe_tension_AT").is(":checked");
				datos.monto = $("#cobranza_informe_monto").val();
				if($("#cobranza_informe_proceso").val()!="TODO")
				{
					datos.proceso =  $("#cobranza_informe_proceso").val();
				}
				if($("#cobranza_informe_ruta").val()!="TODO")
				{
					datos.ruta =  $("#cobranza_informe_ruta").val();
				}
				if($("#cobranza_informe_comuna").select2('val')!="TODO")
				{
					datos.comuna =  $("#cobranza_informe_comuna").select2('val');
				}
			    abrir_dialog_message("Generando Archivo...","loading",{});
				$.post
				(
					api_general+"/cbz_proceso/cu_masivo",
					datos,
					function(data)
					{
			    		cerrar_dialog_message();
			    		abrir_dialog_message("Archivo Generado Correctamente","aceptar");
						inicia_cobranza();
					}
					, "json"
				)
				.fail(function(response) {
			    	cerrar_dialog_message();
			    	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
				});
				// abrir_dialog_message("Generación de Archivo deshabilitada temporalmente...","alerta");
			}
		}
		else
		{
			if($("#cobranza_informe_form_individual").validationEngine("validate"))
			{

				var formData = new FormData();
				formData.append('servicio', $("#cobranza_n_servicio").val());
				formData.append('motivo', $("input[name='cobranza_motivo_individual']:checked").val());
				
				var files_selected = $("#cobranza_motivo_individual_archivo")[0].files;
				if(files_selected!=undefined)
				{		
					var files_selected = $("#cobranza_motivo_individual_archivo")[0].files[0];
					formData.append('file0', files_selected);
					formData.append('glosa', $("#cobranza_motivo_individual_archivo_obs").val());
				}

			    var xhr = new XMLHttpRequest();
			    xhr.open('POST',api_general+"/cbz_proceso/cu_individual", true);
				xhr.onreadystatechange = function (oEvent) {  
				    if (xhr.readyState === 4) {  
				        if (xhr.status === 200) {  
				        	cerrar_dialog_message();
				    		abrir_dialog_message("Archivo Generado Correctamente","aceptar");
							inicia_cobranza();
				        } else {  
				        	cerrar_dialog_message();
				        	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
				        }  
				    }  
				}; 

			    abrir_dialog_message("Generando Archivo...","loading",{});
			    xhr.send(formData);
			}
		}
		
	}
});
