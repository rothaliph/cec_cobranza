
$(document).on("click",".dropdown-link-menu",function()
{
	$(this).parent().next().toggle();
	if($(this).hasClass("open-dropdown"))
	{
		$(this).removeClass("open-dropdown");
	}
	else
	{
		$(this).addClass("open-dropdown");
	}
});

$(".sidebar").mouseleave(function()
{
	$.each($(this).find("ul li"),function(i,obj)
	{
		$(obj).removeClass("open-dropdown");
		if($(obj).hasClass("submenu"))
		{
			$(obj).hide();
		}
	});
});

$(document).on("click",".menu-option",function()
{
    url = $(this).data("url");
    if(url!="#")
    {
        contenidoPrincipal(url);        
        fn = $(this).data("fn");
        eval(fn).call();    
    }
    else
    {
        fn = $(this).data("fn");
        eval(fn).call();    
    }
});

$(document).on("click",".submenu-option",function()
{
    url = $(this).data("url");
    if(url!="#")
    {
        contenidoSubPrincipal(url);
    }
    $.each($(".submenu-option"),function(i,obj)
    {
        $(obj).removeClass("submenu-actual");
    });
    $(this).addClass("submenu-actual");

    fn = $(this).data("fn");
    if(fn!=undefined)
    {
    	eval(fn).call();    
    }
});

function cambia_submenu_actual(id)
{
    $.each($(".submenu-option"),function(i,obj)
    {
        $(obj).removeClass("submenu-actual");
    });
    $("#"+id).addClass("submenu-actual");
}

// ----------------------------------------------------------------------------------
