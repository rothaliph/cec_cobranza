var dt_generar_reposicion_masiva = undefined;

function inicia_listar_generar_reposicion()
{
	$("#div_generar_reposicion_listar_tabla_masiva").hide();
	$("#div_generar_reposicion_listar_tabla_individuales").show();

	var permiso_new = getJSON(api_general+"usuarios/get_permiso",{id_ui:'generar_reposicion_listar_agregar'});
	var permiso_new_individual = getJSON(api_general+"usuarios/get_permiso",{id_ui:'generar_reposicion_listar_agregar_individual'});
	if(permiso_new_individual)
	{
		$("#botonera_superior").append('<button type="button" id="generar_reposicion_listar_agregar_individual" data-url="views/cobranza/generar_datos_reposicion_individual.php" class="btn btn-xl btn-accion" ><i class="fas fa-plus"></i> Generar Reposicion Individual</button>');
	}	
	if(permiso_new)
	{
		$("#botonera_superior").append('<button type="button" id="generar_reposicion_listar_agregar" data-url="views/cobranza/generar_datos_reposiciones.php" class="btn btn-xl btn-accion" ><i class="fas fa-plus"></i> Generar Reposiciones</button>');
	}	
	genera_datatable_individual_generar_reposicion();
	// genera_datatable_masiva_generar_reposicion();
}

function inicia_generar_reposicion()
{
	$("#generar_reposicion_informe_form_masivo").validationEngine({autoHidePrompt:true, autoHideDelay:2500});
}

function inicia_generar_reposicion_individual()
{
	$("#generar_reposicion_individua_linforme_form_individual").validationEngine({autoHidePrompt:true, autoHideDelay:2500});

}

$(document).on("blur","#generar_reposicion_individual_n_servicio",function()
{
	if($(this).val()!="")
	{
		var datos = {};
		datos.cuenta =  $(this).val();
			
		var data = getJSON(api_general+"cuentas/get",datos);
		if(data.length != 0)
		{
			$("#generar_reposicion_individual_nombre_cliente").val(data.nombre);
			$("#generar_reposicion_individual_direccion_cliente").val(data.direccion);
		}
		else
		{
			$("#generar_reposicion_individual_n_servicio").val("");
			$("#generar_reposicion_individual_nombre_cliente").val("");
			$("#generar_reposicion_individual_direccion_cliente").val("");
			$("#generar_reposicion_individual_n_servicio").focus();
			$("#generar_reposicion_individual_n_servicio").select();
			
		}
	}
	else
	{
		$("#generar_reposicion_individual_nombre_cliente").val("");
		$("#generar_reposicion_individual_direccion_cliente").val("");
	}
});

$(document).on("change","#generar_reposicion_listar_select",function()
{
	if($(this).val()=="1")
	{
		$("#div_generar_reposicion_listar_tabla_masiva").show();
		$("#div_generar_reposicion_listar_tabla_individuales").hide();
		genera_datatable_masiva_generar_reposicion();
	}
	else
	{
		$("#div_generar_reposicion_listar_tabla_individuales").show();
		$("#div_generar_reposicion_listar_tabla_masiva").hide();
		genera_datatable_individual_generar_reposicion();

	}
});


$(document).on("click","#generar_reposicion_volver",function()
{
	contenidoSubPrincipal($("#reposicion_generar_submenu_listar").data("url"));
	inicia_listar_generar_reposicion();
});

$(document).on("click","#generar_reposicion_listar_agregar",function()
{
	contenidoSubPrincipal($(this).data("url"));
	inicia_generar_reposicion();
});

$(document).on("click","#generar_reposicion_listar_agregar_individual",function()
{
	contenidoSubPrincipal($(this).data("url"));
	inicia_generar_reposicion_individual();
});

$(document).on("click","#generar_reposicion_individual_guardar",function()
{
	if($("#generar_reposicion_individua_linforme_form_individual").validationEngine("validate"))
	{

		var formData = new FormData();
		formData.append('servicio', $("#generar_reposicion_individual_n_servicio").val());
		
		var files_selected = $("#generar_reposicion_individual_archivo")[0].files;
		if(files_selected!=undefined)
		{		
			var files_selected = $("#generar_reposicion_individual_archivo")[0].files[0];
			formData.append('file0', files_selected);
			formData.append('glosa', $("#generar_reposicion_individual_archivo_obs").val());
		}

	    var xhr = new XMLHttpRequest();
	    xhr.open('POST',api_general+"cbz_proceso/cu_individual_reposicion", true);
		xhr.onreadystatechange = function (oEvent) {  
		    if (xhr.readyState === 4) {  
		        if (xhr.status === 200) {  
		        	cerrar_dialog_message();
					contenidoSubPrincipal($("#reposicion_generar_submenu_listar").data("url"));
					inicia_listar_generar_reposicion();
		        } else {  
		        	cerrar_dialog_message();
		        	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
		        }  
		    }  
		}; 

	    abrir_dialog_message("Generando Archivo...","loading",{});
	    xhr.send(formData);
	}
});


$(document).on("click","#generar_reposicion_guardar",function()
{
	if($("#generar_reposicion_informe_form_masivo").validationEngine("validate"))
	{
	 //    abrir_dialog_message("Generando Archivo...","loading",{});
		// $.post
		// (
		// 	api_general+"cbz_proceso/cu_masivo_reposicion",
		// 	{},
		// 	function(data)
		// 	{
	 //    		cerrar_dialog_message();
		// 		contenidoSubPrincipal($("#reposicion_generar_submenu_listar").data("url"));
		// 		inicia_listar_generar_reposicion();
		// 	}
		// 	, "json"
		// )
		// .fail(function(response) {
	 //    	cerrar_dialog_message();
	 //    	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
		// });
		abrir_dialog_message("Generación de Archivo deshabilitada temporalmente...","alerta");
	}
});

function genera_datatable_masiva_generar_reposicion()
{
	var columnDefs = [
	{
	    // "targets": [3],
	    // "className": "txtr",
	    // "width": "4%"
	}];

	if(dt_generar_reposicion_masiva != undefined)
	{
		dt_generar_reposicion_masiva.destroy();
	}
	var permiso_ver = getJSON(api_general+"usuarios/get_permiso",{id_ui:'generar_reposicion_listar_ver'});
	dt_generar_reposicion_masiva = iniciar_datatable_basica("generar_reposicion_listar_tabla_masiva",api_general+"cbz_proceso/sp_masivo_reposicion",function(valor){
		var id_generar_reposicion = valor[0];
		valor[0] = valor[1];
		var btnes = "";
		if(permiso_ver)
		{
			btnes += "<button type='button' class='btn btn-xs btn-accion' id='generar_reposicion_listar_ver' title='Ver' data-url='views/cobranza/generar_cobranza.php' value='"+id_generar_reposicion+"'><i class='far fa-file-pdf'></i></button>";;
		}

		valor[1] = btnes;
		return valor;
	},undefined,undefined,true,columnDefs);
	$("#generar_reposicion_listar_tabla_masiva_wrapper .ui-corner-tr").append("<div class='datatable_title_custom'><label>Listado de Generaciones de Datos para Reposicion</label></div>");
}
		

function genera_datatable_individual_generar_reposicion()
{
	var columnDefs = [
	{
	    // "targets": [2],
	    // "className": "txtr",
	}];
	if(dt_generar_reposicion_masiva != undefined)
	{
		dt_generar_reposicion_masiva.destroy();
	}
	var permiso_ver = getJSON(api_general+"usuarios/get_permiso",{id_ui:'generar_reposicion_listar_ver'});
	var permiso_adjunto = getJSON(api_general+"usuarios/get_permiso",{id_ui:'generar_reposicion_listar_adjunto'});
	dt_generar_reposicion_masiva = iniciar_datatable_basica("generar_reposicion_listar_tabla_individuales",api_general+"cbz_proceso/sp_individual_reposicion",function(valor){
		var generar_reposicion = valor[4];
		var id_adjunto = valor[3];
		var item = Array();
		item.push(valor[0]);
		item.push(valor[1]);
		item.push(valor[2]);
		// item.push(valor[3]);

		var btnes = "";
		if(permiso_adjunto)
		{
			if(!isEmpty(id_adjunto))
			{
					btnes += "<button type='button' class='btn btn-xs btn-accion' id='generar_reposicion_listar_adjunto' title='Descargar Adjunto' value='"+id_adjunto+"'><i class='fas fa-paperclip'></i></button>";;
			}
		}
		
		if(permiso_ver)
		{
			btnes += "<button type='button' class='btn btn-xs btn-accion' id='generar_reposicion_listar_ver' title='Ver' data-url='views/cobranza/generar_cobranza.php' value='"+generar_reposicion+"'><i class='far fa-file-pdf'></i></button>";;
		}

		item.push(btnes);
		return item;
	},undefined,undefined,true,columnDefs);
	$("#generar_reposicion_listar_tabla_individuales_wrapper .ui-corner-tr").append("<div class='datatable_title_custom'><label>Listado de Generaciones de Datos para Reposicion</label></div>");
}


$(document).on("click","#generar_reposicion_listar_adjunto",function()
{
	window.open(api_general	+ "archivo/get/?"+$(this).val());
});