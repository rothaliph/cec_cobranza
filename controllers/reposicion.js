var dt_reposicion = undefined;
var dt_estado_reposicion = undefined;
var reposicion_doc_actual = 0;
var reposicion_cuentas_datos = Array();
var reposicion_registro_actual = 0;
var estado_por_defecto_reposicion = 0;


function inicia_listar_reposiciones()
{
	// if(dt_reposicion != undefined)
	// {
	// 	dt_reposicion.destroy();
	// }
	// // var permiso_edit = getJSON(api_general+"usuarios/get_permiso",{id_ui:'reposicion_listar_editar'});
	// dt_reposicion = iniciar_datatable_basica("reposicion_listar_tabla",api_general+"cbz_reposicion/sp",function(valor){
	// 	var id_reposicion = valor[3];

	// 	var btnes = "";
	// 	// if(permiso_edit)
	// 	{
	// 		btnes += "<button type='button' class='btn btn-xs btn-accion' id='reposicion_listar_edit' title='Editar' data-url='views/cobranza/ingresar_reposicion.php' value='"+id_reposicion+"'><i class='far fa-edit'></i></button>";;
	// 	}

	// 	valor[3] =btnes;
	// 	return valor;
	// },undefined,undefined,true,columnDefs);
	// var permiso_new = getJSON(api_general+"usuarios/get_permiso",{id_ui:'reposicion_listar_agregar'});
	// if(permiso_new)
	// {
		// $("#botonera_superior").append('<button type="button" id="reposicion_listar_agregar" data-url="views/cobranza/ingresar_reposicion.php" class="btn btn-xl btn-accion" ><i class="fas fa-plus"></i> Registrar reposicions</button>');
	// }	
}

// $(document).on("click","#reposicion_listar_agregar",function()
// {
// 	contenidoSubPrincipal($(this).data("url"));
// 	inicia_reposicion();
// });

function inicia_reposicion(id_asignacion)
{
	reposicion_doc_actual = id_asignacion;
	$("#reposicion_ingresar_cabecera_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#reposicion_ingresar_cabecera_cuenta_form").validationEngine({binded:false, autoHidePrompt:true, autoHideDelay:6500});
	$("#reposicion_ingresar_cuenta_hora").datetimepicker({format:"H:i", datepicker:false, mask:true, defaultTime: "now"});
	$("#reposicion_ingresar_cuenta_fecha").datetimepicker({format:"d/m/Y", timepicker:false, mask:true, defaultDate:new Date()});
	
	reposicion_cuentas_datos = Array();
	reposicion_registro_actual = 0;
	reposicion_ingresar_registro_deshabilitar_inputs();

	var estados_de_reposicion = getJSON(api_general+"cbz_asignacion_estado/s2",{tipo:"R"});
	$("#reposicion_ingresar_cuenta_estado").select2({ data: estados_de_reposicion });
	estado_por_defecto_reposicion = "";
	$.each(estados_de_reposicion,function(i,estado)
	{
		if(estado.data.finaliza=="S")
		{
			estado_por_defecto_reposicion = estado.id;
		}
	});



	var datos = getJSON(api_general+"cbz_asignacion/datos_asignado",{codigo:id_asignacion});
	
	$("#reposicion_ingresar_contratista").val(datos.contratista);
	$("#reposicion_ingresar_movil").val(datos.movil);

	$.each(datos.cuentas,function(i,val)
	{
		var item = {};
		item.n_cuenta = val[0];
		item.nom_cliente = val[1];
		item.tarifa = val[2];
		item.motivo = val[3];
		item.ruta = val[4];
		item.proceso = val[5];
		item.nom_estado = val[6];
		item.id_estado = val[7];
		item.id_registro = val[8];
		item.permite_cambio_estado = val[9];
		item.modificado = "N";

		item.hora = val[10];
		item.glosa = val[11];
		item.n_sello = val[12];
		item.fecha = val[13];


		reposicion_cuentas_datos.push(item);
	});
	reposicion_ingresar_actualiza_datos_tabla();
}

$(document).on("change",".reposicion_ingresar_check_asignar",function()
{
	if($(this).data("asignado")!='')
	{
		if($(this).prop('checked'))
		{		
			id_check_actual_revisado = $(this).val();
			abrir_dialog_message("Está reasignando una cuenta<br> ¿está seguro?","alerta",[{text:"Aceptar", click:reposicion_ingresar_check_asignar_aceptar},{text:"Cancelar", click:reposicion_ingresar_check_asignar_cancelar}]);
		}
	}
});

function reposicion_ingresar_actualiza_datos_tabla()
{
	if(dt_estado_reposicion != undefined)
	{
		dt_estado_reposicion.destroy();
	}
	$("#reposicion_ingresar_cuenta_tabla> tbody tr").remove();
	$.each(reposicion_cuentas_datos,function(i,val)
	{
		var id_detalle = val.id_registro;
		var row = "<tr>";
		row += "<td>"+val.n_cuenta+"</td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.nom_cliente+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.tarifa+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.motivo+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.ruta+"' readonly></td>";
		row += "<td><input type='text' class='col-xs-12' value='"+val.proceso+"' readonly></td>";
		if(val.id_estado!="")
		{
			row += "<td><span class='label label-info'>"+val.nom_estado+" <i class='fas fa-history'></i></span></td>";
		}
		else
		{
			row += "<td><span class='label label-danger'>Sin registro <i class='fas fa-exclamation-triangle'></i></span></td>";
		}
	
		row += "<td>";
		if(val.permite_cambio_estado =="S")
		{
			row += "	<button type='button' id='reposicion_listar_item_cargar' value='"+val.id_registro+"' class='btn btn-sm btn-success' ><i class='fas fa-plus'></i></button>";
		}
		row += "</td>";
		row += "</tr>";
		agregarFilaATabla("reposicion_ingresar_cuenta_tabla",row);
	});	
	// dt_estado_reposicion = iniciar_datatable_basica_vacia("reposicion_ingresar_cuenta_tabla",true);
	dt_estado_reposicion = $('#reposicion_ingresar_cuenta_tabla').DataTable( 
    {
    	"paging": false,
    	"searching": true,
        "bSort": false,
    	"info": false,
        "oLanguage": es_leng,
        // "order" : [[0,"DESC"]],
    });

    $("#reposicion_ingresar_cuenta_tabla_wrapper .ui-corner-tr").html("<div class='datatable_title_custom'><label>Listado de Estados de Cuentas para Reposicion</label></div>");

    $("#reposicion_ingresar_cuenta_tabla_filter").addClass("pull-left");
}

$(document).on("click","#reposicion_listar_item_cargar",function()
{
	reposicion_ingresar_cargar_registro($(this).val());
});

$(document).on("click","#reposicion_ingresar_guardar",function()
{
	reposicion_ingresar_guardar_datos(false);
});

$(document).on("click","#reposicion_ingresar_guardar_continuar",function()
{
	reposicion_ingresar_guardar_datos(true);
});

$(document).on("click","#reposicion_ingresar_volver",function()
{
	reposicion_ingresar_volver();
	// abrir_dialog_message("Volverá a la lista de asignaciones y perderá los datos que no hayan sido guardados.<br> ¿Está seguro?","alerta",[{text:"Aceptar", click:reposicion_ingresar_volver},{text:"Cancelar", click:cerrar_dialog_message}]);
});

function reposicion_ingresar_volver()
{
	contenidoSubPrincipal($("#asignacion_reposicion_submenu_listar").data("url"));
	inicia_listar_reposiciones_asignaciones();
	cerrar_dialog_message();
}


function reposicion_ingresar_guardar_datos(continuar)
{
	// if($("#reposicion_ingresar_cabecera_form").validationEngine('validate'))
	{
		datos = {};
		if(reposicion_doc_actual!=0)
		{
			datos.id = reposicion_doc_actual;
		}

		var items = Array();
		$.each(reposicion_cuentas_datos,function(i,val)
		{
			if(val.modificado=="S")
			{			
				var item = {};

				item.id = val.id_registro;
				item.fecha = val.fecha;
				item.hora = val.hora;
				item.estado = val.id_estado;
				item.sello = val.n_sello;
				item.observacion = val.glosa;
				items.push(item);
			}
		});
		datos.items = items;


	    abrir_dialog_message("Guardando Datos...","loading",{});
		$.post
		(
			api_general+"cbz_asignacion/cambio_estado",
			datos,
			function(data)
			{
				reposicion_doc_actual = data;
	    		cerrar_dialog_message();
	    		abrir_dialog_message("Datos Guardados Correctamente","aceptar");

	    		if(!continuar)
	    		{    			
	    			contenidoSubPrincipal($("#asignacion_reposicion_submenu_listar").data("url"));
					inicia_listar_reposiciones_asignaciones();
					cerrar_dialog_message();
	    		}
	    		else
	    		{
	    			contenidoSubPrincipal("views/cobranza/ingresar_reposicion.php");
	    			inicia_reposicion(reposicion_doc_actual);
	    		}
			}
			, "json"
		)
		.fail(function(response) {
	    	cerrar_dialog_message();
	    	abrir_dialog_message("Ocurrió un error inesperado durante el proceso. Revise los datos y reintente.","error");
		});
	}
}

function reposicion_ingresar_registro_deshabilitar_inputs()
{
	$("#reposicion_ingresar_cuenta_editar_aceptar").prop("disabled",true);
	$("#reposicion_ingresar_cuenta_editar_cancelar").prop("disabled",true);
	$("#reposicion_ingresar_cuenta_estado").prop("disabled",true);
	$("#reposicion_ingresar_cuenta_observacion").prop("disabled",true);
	$("#reposicion_ingresar_cuenta_fecha").prop("disabled",true);
	$("#reposicion_ingresar_cuenta_hora").prop("disabled",true);
	$("#reposicion_ingresar_cuenta_sello").prop("disabled",true);

	$("#reposicion_ingresar_cuenta_estado").select2("val","");
	$("#reposicion_ingresar_cuenta_observacion").val("");
	$("#reposicion_ingresar_cuenta_fecha").val("");
	$("#reposicion_ingresar_cuenta_hora").val("");
	$("#reposicion_ingresar_cuenta_sello").val("");
	$("#reposicion_ingresar_cuenta_numero").val("");
	$("#reposicion_ingresar_cuenta_rut").val("");
	$("#reposicion_ingresar_cuenta_nombre").val("");
	$("#reposicion_ingresar_cuenta_direccion").val("");
	$("#reposicion_ingresar_cuenta_tarifa").val("");		
}
function reposicion_ingresar_registro_habilitar_inputs()
{
	$("#reposicion_ingresar_cuenta_editar_aceptar").prop("disabled",false);
	$("#reposicion_ingresar_cuenta_editar_cancelar").prop("disabled",false);
	$("#reposicion_ingresar_cuenta_estado").prop("disabled",false);
	$("#reposicion_ingresar_cuenta_observacion").prop("disabled",false);
	$("#reposicion_ingresar_cuenta_fecha").prop("disabled",false);
	$("#reposicion_ingresar_cuenta_hora").prop("disabled",false);
	$("#reposicion_ingresar_cuenta_sello").prop("disabled",false);
}

$(document).on("click","#reposicion_ingresar_cuenta_editar_cancelar",function()
{
	reposicion_ingresar_registro_deshabilitar_inputs();
});

$(document).on("click","#reposicion_ingresar_cuenta_editar_aceptar",function()
{
	if($("#reposicion_ingresar_cabecera_cuenta_form").validationEngine('validate'))
	{
		var registro = {};
		var nuevos_datos = Array();
		$.each(reposicion_cuentas_datos,function(i,val)
		{
			if(val.id_registro == reposicion_registro_actual)
			{
				val.id_estado = $("#reposicion_ingresar_cuenta_estado").val();
				val.nom_estado = $("#reposicion_ingresar_cuenta_estado").select2("data").text;
				var finaliza_cuenta = $("#reposicion_ingresar_cuenta_estado").select2("data").data.finaliza;
				// val.permite_cambio_estado = finaliza_cuenta=="S"? "N": "S"; //si finaliza, no permite cambio estado, y vice versa
				val.fecha = $("#reposicion_ingresar_cuenta_fecha").val()!="" ? $("#reposicion_ingresar_cuenta_fecha").val() : $("#reposicion_ingresar_cuenta_fecha").val(); // MODIFICAR PARA GUARDAR FECHA ACTUAL
				val.hora = $("#reposicion_ingresar_cuenta_hora").val();
				val.n_sello = $("#reposicion_ingresar_cuenta_sello").val();
				val.glosa = $("#reposicion_ingresar_cuenta_observacion").val();
				val.modificado = "S";
			}
			nuevos_datos.push(val);
		});
		reposicion_cuentas_datos = nuevos_datos;

		reposicion_ingresar_actualiza_datos_tabla();
		reposicion_ingresar_registro_deshabilitar_inputs();
		$("#reposicion_ingresar_cuenta_estado").select2("val","");
		$("#reposicion_ingresar_cuenta_observacion").val("");
		$("#reposicion_ingresar_cuenta_fecha").val("");
		$("#reposicion_ingresar_cuenta_hora").val("");
		$("#reposicion_ingresar_cuenta_sello").val("");

		$("#reposicion_ingresar_cuenta_numero").focus();

		reposicion_ingresar_guardar_datos(true);
		$("#reposicion_ingresar_cuenta_numero").select();
	}
});

function reposicion_ingresar_cargar_registro(id_registro)
{
	reposicion_registro_actual = id_registro;
	var registro = {};
	$.each(reposicion_cuentas_datos,function(i,val)
	{
		if(val.id_registro == id_registro)
		{
			registro = val;
		}
	});




	if(registro.id_estado!="")
	{
		$("#reposicion_ingresar_cuenta_estado").select2("val",registro.id_estado);
	}
	else
	{
		$("#reposicion_ingresar_cuenta_estado").select2("val",estado_por_defecto_reposicion);
	}
	if(registro.hora!="")
	{
		$("#reposicion_ingresar_cuenta_hora").val(registro.hora);
	}
	else
	{
		$("#reposicion_ingresar_cuenta_hora").datetimepicker({value:new Date()});
	}
	if(registro.fecha!="")
	{
		$("#reposicion_ingresar_cuenta_fecha").val(registro.fecha);
	}
	else
	{
		$("#reposicion_ingresar_cuenta_fecha").datetimepicker({value:new Date()});
	}
	$("#reposicion_ingresar_cuenta_sello").val(registro.n_sello);
	$("#reposicion_ingresar_cuenta_observacion").val(registro.glosa);

	var datos = {};
	datos.cuenta = registro.n_cuenta;
	var data = getJSON(api_general+"cuentas/get",datos);

	$("#reposicion_ingresar_cuenta_numero").val(data.cuenta);
	$("#reposicion_ingresar_cuenta_rut").val(data.rut);
	$("#reposicion_ingresar_cuenta_nombre").val(data.nombre);
	$("#reposicion_ingresar_cuenta_direccion").val(data.direccion);
	$("#reposicion_ingresar_cuenta_tarifa").val(data.tarifa);

	reposicion_ingresar_registro_habilitar_inputs();
	$("#reposicion_ingresar_cuenta_estado").select2("open");
}

// 

$(document).on("blur","#reposicion_ingresar_cuenta_numero",function(e)
{
	e.stopPropagation();
	if($(this).val()!="" && $("#reposicion_ingresar_cabecera_form").validationEngine('validate'))
	{
		var cuenta = $("#reposicion_ingresar_cuenta_numero").val();
		var registro = 0;
		$.each(reposicion_cuentas_datos,function(i,val)
		{
			if(val.n_cuenta == cuenta && val.permite_cambio_estado == "S")
			{
				registro = val.id_registro;
			}
		});

		if(registro!=0)
		{
			reposicion_ingresar_cargar_registro(registro);		
		}
		else
		{
			abrir_dialog_message("La cuenta no puede ser cargada debida a que no se encuentra en la lista, o ya tiene un estado que no puede ser modificado","alerta");
			reposicion_ingresar_registro_deshabilitar_inputs();
			$("#reposicion_ingresar_cuenta_numero").select();
		}
	}
});