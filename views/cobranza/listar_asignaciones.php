<div class="form-group header-buttons" id="botonera_superior">
    <!--  -->
</div>


<form role="form" class="form-horizontal">
    <div class="form-group last">
        <div class="col-xs-12">
            <label class="col-xs-1">Mostrar</label>
            <div class="col-xs-2">
                <select class="form-control" id="asignacion_listar_tabla_filtro">
                    <option value="HOY">Sólo HOY</option>
                    <option value="TODOS">Todas</option>
                </select>
            </div>
        </div>
    </div>
</form>

<table id="asignacion_listar_tabla" >
    <thead>
        <tr>
            <th style="width:120px !important;"">Fecha y Hora</th>
            <th>Operador</th>
            <th>Movil</th>
            <th style="width:80px !important;">Vigencia</th>
            <th style="width:80px !important;"></th>
        </tr>
    </thead>
    <tbody>
                        
    </tbody>
</table>