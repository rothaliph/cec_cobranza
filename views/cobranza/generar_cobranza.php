<form class="form-horizontal" role="form" id="cobranza_informe_form_cabecera">
			<div class="form-group">
		        <label class="col-xs-2">Tipo de Corte</label>
				<div class="col-xs-2">
		            <select class="form-control validate[required]" id="cobranza_tipo_corte" type="text">
		            	<option value="">Seleccione...</option>
		            	<option value="1">Corte Masivo</option>
		            	<option value="2">Corte Individual</option>
		            </select>
		        </div>
			</div>
		</form>
		<div class="divisor"></div>
		<div id="cobranza_tipo_masivo">
			<form class="form-horizontal" role="form" id="cobranza_informe_form_masivo">
				<div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">Motivo de Corte</label>
				        <div class="col-xs-6">
				            <div class="form-check form-check-inline ">
							  <input class="form-check-input validate[minCheckbox[1]]" name="cobranza_motivo" type="checkbox" value="D" id="cobranza_informe_motivo_deuda">
							  <label class="form-check-label" for="cobranza_informe_motivo_deuda">
							    Deuda
							  </label>
							</div>   
				            <div class="form-check form-check-inline ">
							  <input class="form-check-input validate[minCheckbox[1]]" name="cobranza_motivo" type="checkbox" value="T" id="cobranza_informe_motivo_t1">
							  <label class="form-check-label" for="cobranza_informe_motivo_t1">
							    T1 Vencido
							  </label>
							</div>      
				        </div>
			        </div>
			    </div>
				<div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">Tensión</label>
				        <div class="col-xs-6">
				            <div class="form-check form-check-inline ">
							  <input class="form-check-input validate[minCheckbox[1]]" name="cobranza_tension" type="checkbox" value="BT" id="cobranza_informe_tension_BT">
							  <label class="form-check-label" for="cobranza_informe_tension_BT">
							    Baja
							  </label>
							</div>  
							<div class="form-check form-check-inline ">
							  <input class="form-check-input validate[minCheckbox[1]]" name="cobranza_tension" type="checkbox" value="AT" id="cobranza_informe_tension_AT">
							  <label class="form-check-label" for="cobranza_informe_tension_AT">
							    Media
							  </label>
							</div>     
				        </div>
			        </div>
			    </div>
			    <div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">Monto</label>
				        <div class="col-xs-2">
				            <input class="form-control validate[required,min[0],custom[integer]]" id="cobranza_informe_monto" type="number" placeholder="Monto mínimo"/>
				        </div>
			        </div>
			    </div>
			    <div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">Proceso</label>
				        <div class="col-xs-2">
				            <input class="form-control validate[funcCall[validar_select2_global]]" id="cobranza_informe_proceso">
				        </div>
			        </div>
			    </div>
			    <div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">Ruta</label>
				        <div class="col-xs-2">
				            <input class="form-control validate[funcCall[validar_select2_global]]" id="cobranza_informe_ruta">
				        </div>
			        </div>
			    </div>
			    <div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">Comuna</label>
				        <div class="col-xs-2">
				        	<input class="form-control validate[funcCall[validar_select2_global]]" id="cobranza_informe_comuna">
				            <!-- <select class="form-control" id="cobranza_informe_comuna" type="text">
				            	<option value="0">Seleccione...</option>
				            	<option value="2">TODAS</option>
				            	<option value="1">Comuna 1</option>
				            	<option value="2">Comuna 2</option>
				            	<option value="2">Comuna 3</option>
				            	<option value="2">Comuna 4</option>
				            </select> -->
				        </div>
			        </div>
			    </div>
			</form>
		</div>
		<div id="cobranza_tipo_individual">
			<form class="form-horizontal" role="form" id="cobranza_informe_form_individual">
				<div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">N° de Servicio</label>
				        <div class="col-xs-2">
				            <input class="form-control validate[required,custom[integer]]" type="text" id="cobranza_n_servicio">   
				        </div>
				        <div class="col-xs-4">
				            <input class="form-control" type="text" id="cobranza_nombre_cliente" readonly>   
				        </div>
				        <div class="col-xs-4">
				            <input class="form-control" type="text" id="cobranza_direccion_cliente" readonly>   
				        </div>
			        </div>
			    </div>
				<div class="form-group">
					<div class="col-xs-12">
				        <label class="col-xs-2">Motivo de Corte</label>
				        <div class="col-xs-8">
							<div class="form-check form-check-inline">
								<input class="form-check-input validate[groupRequired[motivo_individual]]" type="radio" name="cobranza_motivo_individual" id="cobranza_informe_motivo_individual_deuda" value="Deuda">
								<label class="form-check-label" for="inlineRadio1">Deuda</label>
							</div>
							<div class="form-check form-check-inline">
								<input class="form-check-input validate[groupRequired[motivo_individual]]" type="radio" name="cobranza_motivo_individual" id="cobranza_informe_motivo_individual_solicitud" value="Solicitud Dueño">
								<label class="form-check-label" for="inlineRadio3">Solicitud Dueño</label>
							</div> 
				        </div>
			        </div>
			    </div>
				<div class="form-group">
					<label class="col-xs-2">Subir Archivo</label>
			        <div class="col-xs-4">
			        	<input class="form-control validate[maxSize[60]]" id="cobranza_motivo_individual_archivo_obs" type="text" placeholder="Comentario del Archivo"/>
			        </div>
			        <div class="col-xs-6">
					    <input type="file" class="custom-file-input" id="cobranza_motivo_individual_archivo">
				  	</div>
			    </div>
			</form>
		</div>

		<div id="div_ticket_botones_principales" class="form-group final-buttons">
			<button type="button" id="cobranza_generar_ot" class="btn btn-xl btn-final-icon btn-accion"><i class="fa fa-floppy-o" aria-hidden="true"></i> Generar OT</button>

		</div>