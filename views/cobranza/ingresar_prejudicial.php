<div id="cobranza_ingresar_prejudicial_tipo_header" class="alert alert-info" role="alert" style="display:none;">
	<form class="form-horizontal" role="form">
		<div class="form-group">
			<div class="col-xs-12">
				<h5 class="col-xs-12 txtc"><strong id="cobranza_ingresar_prejudicial_tipo_header_edit"></strong></h5>
			</div>
		</div>
	</form>
</div>
<form class="form-horizontal" role="form" id="cobranza_ingresar_prejudicial_cabecera_form">
	<div class="form-group">
		<label class="col-xs-2">N° Cuenta</label>
		<div class="col-xs-2"><input class="form-control validate[required,maxSize[45],custom[integer]]" type="text" tabindex="1" id="cobranza_ingresar_prejudicial_cuenta_numero" /></div>
		<button type="button" class="btn btn-xs btn-buscar" id="cobranza_ingresar_prejudicial_cuenta_buscar"><i class="fa fa-search" aria-hidden="true"></i></button>   
	</div>
	<div class="form-group">
		<label class="col-xs-2">Rut</label>
		<div class="col-xs-2"><input class="form-control" type="text"  tabindex="-1" id="cobranza_ingresar_prejudicial_cuenta_rut" readonly/></div>
		<label class="col-xs-2">Cliente</label>
		<div class="col-xs-4"><input class="form-control" type="text"  tabindex="-1" id="cobranza_ingresar_prejudicial_cuenta_nombre" readonly/></div>
	</div>
	<div class="form-group">
		<label class="col-xs-2">Tarifa</label>
		<div class="col-xs-2"><input class="form-control" type="text"  tabindex="-1" id="cobranza_ingresar_prejudicial_cuenta_tarifa" readonly/></div>
		<label class="col-xs-2">Dirección</label>
		<div class="col-xs-4"><input class="form-control" type="text"  tabindex="-1" id="cobranza_ingresar_prejudicial_cuenta_direccion" readonly/></div>
	</div>
	<div class="divisor"></div>
</form>
<form class="form-horizontal" role="form" id="cobranza_ingresar_prejudicial_archivo_form">
	<div class="form-group">
		<label class="col-xs-2">Archivo</label>
		<div class="col-xs-2"><input class="form-control-file validate[]"  tabindex="2" type="file" id="cobranza_ingresar_prejudicial_cuenta_archivo"/></div>
	</div>
	<div class="form-group">
		<label class="col-xs-2">Fecha de Archivo</label>
		<div class="col-xs-2"><input class="form-control validate[required] datepicker"  tabindex="3" type="text" id="cobranza_ingresar_prejudicial_cuenta_fecha" /></div>
	</div>
	<div class="form-group">
		<label class="col-xs-2">Glosa</label>
		<div class="col-xs-8"><input class="form-control validate[required,maxSize[200]]"  tabindex="4" type="text" id="cobranza_ingresar_prejudicial_cuenta_glosa"/></div>
	</div>
	<div class="form-group">
		<label class="col-xs-2">Observación</label>
		<div class="col-xs-8"><input class="form-control validate[maxSize[200]]" type="text"  tabindex="5" id="cobranza_ingresar_prejudicial_cuenta_observacion"/></div>
	</div>
	<div class="form-group final-buttons">
		<button type="button" id="cobranza_ingresar_prejudicial_cuenta_agregar" val="0"  tabindex="6" class="btn btn-xs btn-accion">Añadir Registro</button>
	</div>
	<div class="divisor"></div>
</form>

<table id="cobranza_ingresar_prejudicial_cuenta_tabla" >
    <thead>
        <tr>
            <th style="width:100px !important;">Proceso</th>
            <th style="width:100px !important;">Fecha</th>
            <th>Glosa</th>
            <th>Observación</th>
            <th style="width:110px !important;"></th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>


<div id="botones_principales" class="form-group final-buttons">
	<button tabindex="-1" type="button" id="cobranza_ingresar_prejudicial_cuenta_volver" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-arrow-left"></i> Volver</button>
	<button tabindex="-1" type="button" id="cobranza_ingresar_prejudicial_cuenta_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Guardar</button>
</div>

<div class="col-xs-12" style="display:none;" id="dialog_buscador_prejudicial_cuenta">
	<form class="form-horizontal" role="form" id="form_dialog_buscar_prejudicial_cuenta" >
		<div class="form-group alert alert-info" role="alert" style="margin-bottom: 15px;">
			<div class="col-xs-12">
				<h5 class="col-xs-12 txtc"><strong>Sólo se mostrarán cuentas CORTADAS</strong></h5>
			</div>
		</div>
	    <div class="form-group sin_espaciado">
	    	<label class="col-xs-2 sin_espaciado txtc" >N° Cuenta</label>
	    	<label class="col-xs-2 sin_espaciado txtc" >RUT</label>
	    	<label class="col-xs-4 sin_espaciado txtc" >Nombre</label>
	    	<label class="col-xs-3 sin_espaciado txtc" >Dirección</label>
	    </div>
	    <div class="form-group">
	    	<div class="col-xs-2"><input class="form-control validate[groupRequired[filtrar_ticket],minSize[1],maxSize[45],custom[integer]]" id="buscador_cuenta_ncuenta" type="text" /></div>
	    	<div class="col-xs-2"><input class="form-control validate[groupRequired[filtrar_ticket],minSize[3],maxSize[20]]" id="buscador_cuenta_rut" type="text" /></div>
	    	<div class="col-xs-4"><input class="form-control validate[groupRequired[filtrar_ticket],minSize[3],maxSize[45]]" id="buscador_cuenta_nombre" type="text" /></div>
	    	<div class="col-xs-3"><input class="form-control validate[groupRequired[filtrar_ticket],minSize[3],maxSize[45]]" id="buscador_cuenta_direccion" type="text" /></div>
	    	<div class="col-xs-1"><button type="button" class="btn btn-xs btn-buscar full-width" id="buscador_cuenta_prejudicial_filtrar"><i class="fa fa-search" aria-hidden="true"></i></button> </div>
	    </div>
	</form>
	<div class="divisor"></div>
	<table class="table" id="buscador_cuenta_prejudicial_resultados">
		<thead>
			<tr>
				<th style="width:16.66666667%">N° Cuenta</th>
				<th style="width:16.66666667%">RUT</th>
				<th style="width:33.33333333%">Nombre</th>
				<th style="width:25%">Dirección</th>
				<th style="width:8.33333333%"></th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>