<form class="form-horizontal" role="form" id="generar_reposicion_informe_form_masivo">
	<div class="form-group">
		<div class="col-xs-12">
	        <label class="col-xs-2">Antiguedad de Corte</label>
	        <div class="col-xs-2">
	            <select class="form-control validate[required]" id="generar_reposicion_informe_antiguedad" type="text" readonly>
	            	<option value="6">6 meses</option>
	            </select>
	        </div>
        </div>
    </div>
    <div class="form-group">
		<div class="col-xs-12">
	        <label class="col-xs-2">Reposiciones de</label>
	        <div class="col-xs-2">
	            <select class="form-control validate[required]" id="generar_reposicion_informe_cuentas" type="text" readonly>
	            	<option value="C">Cuentas Cortadas</option>
	            </select>
	        </div>
        </div>
    </div>
</form>


<div id="botones_principales" class="form-group final-buttons">
	<button tabindex="-1" type="button" id="generar_reposicion_volver" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-arrow-left"></i> Volver</button>
	<button tabindex="-1" type="button" id="generar_reposicion_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Generar Reposiciones</button>
</div>