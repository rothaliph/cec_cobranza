<form class="form-horizontal" role="form" id="generar_reposicion_individua_linforme_form_individual">
	<div class="form-group">
		<div class="col-xs-12">
	        <label class="col-xs-2">N° de Servicio</label>
	        <div class="col-xs-2">
	            <input class="form-control validate[required,custom[integer]]" type="text" id="generar_reposicion_individual_n_servicio">   
	        </div>
	        <div class="col-xs-4">
	            <input class="form-control" type="text" id="generar_reposicion_individual_nombre_cliente" readonly>   
	        </div>
	        <div class="col-xs-4">
	            <input class="form-control" type="text" id="generar_reposicion_individual_direccion_cliente" readonly>   
	        </div>
        </div>
    </div>
	<div class="form-group">
		<label class="col-xs-2">Subir Archivo</label>
        <div class="col-xs-4">
        	<input class="form-control validate[maxSize[60]]" id="generar_reposicion_individual_archivo_obs" type="text" placeholder="Comentario del Archivo"/>
        </div>
        <div class="col-xs-6">
		    <input type="file" class="custom-file-input" id="generar_reposicion_individual_archivo">
	  	</div>
    </div>
</form>


<div id="botones_principales" class="form-group final-buttons">
	<button tabindex="-1" type="button" id="generar_reposicion_volver" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-arrow-left"></i> Volver</button>
	<button tabindex="-1" type="button" id="generar_reposicion_individual_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Generar Reposicion</button>
</div>