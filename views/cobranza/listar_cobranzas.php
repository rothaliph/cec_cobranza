<div class="form-group header-buttons" id="botonera_superior">
    <!--  -->
</div>

<form role="form" class="form-horizontal">
    <div class="form-group last">
        <div class="col-xs-12">
            <label class="col-xs-1">Mostrar</label>
            <div class="col-xs-2">
                <select class="form-control" id="cobranza_listar_select">
                    <option value="1">Masivas</option>
                    <option value="2">Individuales</option>
                </select>
            </div>
        </div>
    </div>
</form>

<div id="div_cobranza_listar_tabla_masiva">
    <table id="cobranza_listar_tabla_masiva" >
        <thead>
            <tr>
                <th style="width:120px !important;"">Fecha y Hora</th>
                <th>Motivo</th>
                <th>Tensión</th>
                <th>Deuda</th>
                <th>Proceso</th>
                <th>Ruta</th>
                <th>Comuna</th>
                <th style="width:80px !important;"></th>
            </tr>
        </thead>
        <tbody>
                            
        </tbody>
    </table>
</div>
<div id="div_cobranza_listar_tabla_individuales">
    <table id="cobranza_listar_tabla_individuales" >
        <thead>
            <tr>
                <th style="width:120px !important;"">Fecha</th>
                <th>Cuenta</th>
                <th>Motivo</th>
                <th>Glosa Adjunto</th>
                <th style="width:80px !important;"></th>
            </tr>
        </thead>
        <tbody>
                            
        </tbody>
    </table>
</div>