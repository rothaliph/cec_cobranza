<div class="form-group header-buttons" id="botonera_superior">
    <!--  -->
</div>

<form role="form" class="form-horizontal">
    <div class="form-group last">
        <div class="col-xs-12">
            <label class="col-xs-1">Mostrar</label>
            <div class="col-xs-2">
                <select class="form-control" id="generar_reposicion_listar_select">
                    <option value="2">Individuales</option>
                    <option value="1">Masivas</option>
                </select>
            </div>
        </div>
    </div>
</form>

<div id="div_generar_reposicion_listar_tabla_masiva">
    <table id="generar_reposicion_listar_tabla_masiva" >
        <thead>
            <tr>
                <th>Fecha y Hora</th>
                <th style="width:80px !important;"></th>
            </tr>
        </thead>
        <tbody>
                            
        </tbody>
    </table>
</div>
<div id="div_generar_reposicion_listar_tabla_individuales">
    <table id="generar_reposicion_listar_tabla_individuales" >
        <thead>
            <tr>
                <th style="width:120px !important;"">Fecha</th>
                <th>Cuenta</th>
                <!-- <th>Motivo</th> -->
                <th>Glosa Adjunto</th>
                <th style="width:80px !important;"></th>
            </tr>
        </thead>
        <tbody>
                            
        </tbody>
    </table>
</div>