<form class="form-horizontal" role="form" id="reposicion_anular_cabecera_form">
    <div class="form-group">
        <label class="col-xs-2">Contratista</label>
        <div class="col-xs-6">
            <input class="form-control" type="text" id="reposicion_anular_contratista" tabindex="-1" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-2">Móvil</label>
        <div class="col-xs-6">
            <input class="form-control" type="text" id="reposicion_anular_movil" tabindex="-1" readonly>
        </div>
    </div>
    <div class="divisor"></div>
    <div class="form-group">
        <label class="col-xs-2">Motivo Anulación</label>
        <div class="col-xs-8">
            <textarea class="form-control validate[required, maxSize[90]]" id="reposicion_anular_motivo" rows="3" tabindex="1"></textarea>
        </div>
    </div>
    <div class="divisor"></div>
</form>

<table id="reposicion_anular_cuenta_tabla" >
    <thead>
        <tr>
            <th style="width:60px !important;">N° Cuenta</th>
            <th>Cliente</th>
            <th style="width:50px !important;">Tarifa</th>
            <th style="width:100px !important;">Motivo</th>
            <th style="width:100px !important;">Ruta</th>
            <th style="width:100px !important;">Proceso</th>
            <th style="width:100px !important;">Estado</th>
            <!-- <th style="width:50px !important;"></th> -->
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>


<div id="botones_principales" class="form-group final-buttons">
    <button tabindex="-1" type="button" id="reposicion_anular_volver" class="btn btn-xl btn-final-icon btn-accion pull-left"><i class="fas fa-arrow-left"></i> Volver</button>
	<button tabindex="-1" type="button" id="reposicion_anular_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Anular</button>
<!-- 	<button tabindex="-1" type="button" id="reposicion_anular_guardar_continuar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Guardar y Continuar</button>
	<button tabindex="-1" type="button" id="reposicion_anular_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Guardar y Finalizar</button> -->

</div>