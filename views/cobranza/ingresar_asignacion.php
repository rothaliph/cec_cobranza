<form class="form-horizontal" role="form" id="asignacion_ingresar_cabecera_form">
	<div class="form-group">
        <label class="col-xs-2">Contratista</label>
		<div class="col-xs-6">
            <input class="form-control validate[funcCall[validar_select2_global]]" id="asignacion_ingresar_contratista" tabindex="1">
        </div>
	</div>
	<div class="form-group">
        <label class="col-xs-2">Móvil</label>
		<div class="col-xs-6">
            <input class="form-control validate[funcCall[validar_select2_global]]" id="asignacion_ingresar_operador" tabindex="2">
        </div>
	</div>
</form>
<div class="divisor"></div>
<form role="form" class="form-horizontal" id="asignacion_ingresar_filtros_form">
    <div class="form-group last">
        <table class="tabla">
            <thead>
                <tr>
                    <th style="width:110px !important;">N° Cuenta</th>
                    <th style="width:140px !important;">Tarifa</th>
                    <th style="width:160px !important;">Motivo</th>
                    <th style="width:160px !important;">Proceso</th>
                    <th style="width:160px !important;">Ruta</th>
                    <!-- <th style="width:120px !important;">Asignadas</th> -->
                    <th></th>
                    <!-- <th>Asignado A</th> -->
                </tr>
            </thead>
            <tbody>
                <td><input class="form-control validate[requiredGroup[filtros_asignacion],custom[integer]]" id="asignacion_ingresar_filtro_cuenta" tabindex="3"></td>
                <td>
                    <select class="form-control validate[requiredGroup[filtros_asignacion]]" id="asignacion_ingresar_filtro_tarifa" tabindex="4">
                        <option value="TODO">Todas</option>
                        <option value="AT">Media Tensión</option>
                        <option value="BT">Baja Tensión</option>
                    </select>
                </td>
                <td>
                    <select class="form-control validate[requiredGroup[filtros_asignacion]]" id="asignacion_ingresar_filtro_motivo" tabindex="5">
                        <option value="TODO">Todos</option>
                        <option value="D">Deuda</option>
                        <option value="T">TE1 Vencido</option>
                        <option value="S">Solicitud Dueño</option>
                    </select>
                </td>
                <td><input class="form-control validate[funcCall[validar_select2_global]]" id="asignacion_ingresar_filtro_proceso" tabindex="6"></td>
                <td><input class="form-control validate[funcCall[validar_select2_global]]" id="asignacion_ingresar_filtro_ruta" tabindex="7"></td>
                <!-- <td>
                    <select class="form-control" id="asignacion_ingresar_filtro_asignadas" tabindex="8">
                        <option value="TODO">Todas</option>
                        <option value="A">Asignadas</option>
                        <option value="SA">Sin Asignar</option>
                    </select>
                </td> -->
                <td>
                    <button type="button" class="btn btn-xs btn-success col-xs-6" id="asignacion_ingresar_filtro_filtrar" tabindex="9"><i class="fa fa-search"></i> Filtrar</button>
                    <button type="button" class="btn btn-xs btn-info col-xs-6" id="asignacion_ingresar_filtro_limpiar" tabindex="9"><i class="fas fa-times"></i> Limpiar</button>
                </td>
            </tbody>
        </table>
    </div>

</form>
<table id="asignacion_ingresar_cuenta_tabla" class="scroll">
    <thead>
        <tr>
            <th style="width:60px !important;">N° Cuenta</th>
            <th>Cliente</th>
            <th style="width:50px !important;">Tarifa</th>
            <th style="width:100px !important;">Motivo</th>
            <th style="width:100px !important;">Proceso</th>
            <th style="width:100px !important;">Ruta</th>
            <th style="width:50px !important;">Seleccion</th>
            <th style="width:50px !important;">Asignado</th>
            <!-- <th>Asignado A</th> -->
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>


<div id="botones_principales" class="form-group final-buttons">
	<button tabindex="-1" type="button" id="asignacion_ingresar_volver" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-arrow-left"></i> Volver</button>
	<button tabindex="-1" type="button" id="asignacion_ingresar_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Guardar</button>
	<!-- <button tabindex="-1" type="button" id="asignacion_ingresar_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Guardar y Finalizar</button> -->

</div>