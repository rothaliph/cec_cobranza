<form class="form-horizontal" role="form" id="reposicion_ingresar_cabecera_form">
    <div class="form-group">
        <label class="col-xs-2">Contratista</label>
        <div class="col-xs-6">
            <input class="form-control" type="text" id="reposicion_ingresar_contratista" tabindex="-1" readonly>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-2">Móvil</label>
        <div class="col-xs-6">
            <input class="form-control" type="text" id="reposicion_ingresar_movil" tabindex="-1" readonly>
        </div>
    </div>
    <div class="divisor"></div>
    <div class="form-group">
        <label class="col-xs-2">N° Cuenta</label>
        <div class="col-xs-2"><input class="form-control validate[required,custom[integer]]" type="text" id="reposicion_ingresar_cuenta_numero" tabindex="1"/></div>
        <button type="button" class="btn btn-xs btn-buscar" id="reposicion_ingresar_cuenta_buscar" tabindex="-1"><i class="fa fa-search" aria-hidden="true"></i></button>   
    </div>
    <div class="form-group">
        <label class="col-xs-2">Rut</label>
        <div class="col-xs-2"><input class="form-control" type="text" id="reposicion_ingresar_cuenta_rut" tabindex="-1" readonly/></div>
        <label class="col-xs-2">Cliente</label>
        <div class="col-xs-4"><input class="form-control" type="text" id="reposicion_ingresar_cuenta_nombre" tabindex="-1" readonly/></div>
    </div>
    <div class="form-group">
        <label class="col-xs-2">Tarifa</label>
        <div class="col-xs-2"><input class="form-control" type="text" id="reposicion_ingresar_cuenta_tarifa" tabindex="-1" readonly/></div>
        <label class="col-xs-2">Dirección</label>
        <div class="col-xs-4"><input class="form-control" type="text" id="reposicion_ingresar_cuenta_direccion" tabindex="-1" readonly/></div>
    </div>
    <div class="divisor"></div>
</form>
<form class="form-horizontal" role="form" id="reposicion_ingresar_cabecera_cuenta_form">
    <div class="form-group">
        <label class="col-xs-2">Estado</label>
        <div class="col-xs-2"><input class="form-control validate[funcCall[validar_select2_global]]" type="text" id="reposicion_ingresar_cuenta_estado" tabindex="2"/></div>
        <label class="col-xs-2">Observación</label>
        <div class="col-xs-4"><input class="form-control validate[maxSize[200]]" type="text" id="reposicion_ingresar_cuenta_observacion" tabindex="3"/></div>
    </div>
    <div class="form-group">
        <div id="div_reposicion_ingresar_cuenta_fecha" class="col-xs-4">
            <label class="col-xs-6">Fecha</label>
            <div class="col-xs-6"><input class="form-control validate[required] datepicker" type="text" id="reposicion_ingresar_cuenta_fecha" tabindex="3"/></div>
        </div>    
        <label class="col-xs-2">Hora</label>
        <div class="col-xs-2"><input class="form-control validate[required] datepicker" type="text" id="reposicion_ingresar_cuenta_hora" tabindex="4"/></div>
        <label class="col-xs-2">N° Sello</label>
        <div class="col-xs-2"><input class="form-control validate[custom[integer]]" type="text" id="reposicion_ingresar_cuenta_sello" tabindex="5" /></div>
    </div>
    <div class="form-group last">
        <div class="col-xs-4"></div>
        <div class="col-xs-2"><button type="button" class="btn btn-xs btn-success" id="reposicion_ingresar_cuenta_editar_aceptar" tabindex="6"><i class="fas fa-check"></i> Ingresar Estado de Reposición</button></div>
        <div class="col-xs-1"></div>
        <div class="col-xs-2"><button type="button" class="btn btn-xs btn-danger pull-left" id="reposicion_ingresar_cuenta_editar_cancelar" tabindex="7"><i class="fas fa-times"></i> Cancelar</button></div>
        <div class="col-xs-3"></div>
    </div>
    <div class="divisor"></div>    
</form>

<table id="reposicion_ingresar_cuenta_tabla" >
    <thead>
        <tr>
            <th style="width:60px !important;">N° Cuenta</th>
            <th>Cliente</th>
            <th style="width:50px !important;">Tarifa</th>
            <th style="width:100px !important;">Motivo</th>
            <th style="width:100px !important;">Ruta</th>
            <th style="width:100px !important;">Proceso</th>
            <th style="width:100px !important;">Estado</th>
            <th style="width:50px !important;"></th>
        </tr>
    </thead>
    <tbody>

    </tbody>
</table>


<div id="botones_principales" class="form-group final-buttons">
	<button tabindex="-1" type="button" id="reposicion_ingresar_volver" class="btn btn-xl btn-final-icon btn-accion pull-left"><i class="fas fa-arrow-left"></i> Volver</button>
<!-- 	<button tabindex="-1" type="button" id="reposicion_ingresar_guardar_continuar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Guardar y Continuar</button>
	<button tabindex="-1" type="button" id="reposicion_ingresar_guardar" class="btn btn-xl btn-final-icon btn-accion"><i class="fas fa-save"></i> Guardar y Finalizar</button> -->

</div>