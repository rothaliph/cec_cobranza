<?php

require_once '../../api/php/medoo/medoo.php';
require_once('../../api/funciones.php');
require_once('../../api/db/conx_db.php');
// require_once('../api/funciones.php');
require_once('../../cec_essentials/plugins/tcpdf/tcpdf.php');

global $conx;

define('FPDF_FONTPATH', 'font/');

class DITORIUMPDF extends TCPDF {
    public function Footer() {
        $this->SetY(-15);
        $this->SetFont('helvetica', 'I', 8);
        $this->Cell(0, 10, 'Página '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
    }
}





$pdf = new DITORIUMPDF('L', 'mm', 'letter');
$pdf->SetPrintHeader(false);
$pdf->SetPrintFooter(true);
$pdf->SetMargins(10, 5, 10);


function agregar_padding_top($text,$padding=3)
{
	return '<div style="line-height:'.$padding.'px">'.$text.'</div>';
}

function agregar_pagina_pdf($pdf,$f_desde,$vdias,$pub_user3,$fecha_generacion,$es_primera_pagina)
{
	$pdf->AddPage();

	if($es_primera_pagina)
	{	
		$pdf->Image('logo.png', 20, 10, 55, 13, '', '');
		$pdf->setFontSize( 8);
		$pdf->Cell(5, 5, '', 0, 0, 'C');
		$pdf->Ln();
		$pdf->setFontSize( 8);
		$pdf->Cell(65, 4, '', 0, 0, 'C');
		$pdf->setFontSize( 12);
		$pdf->Cell(124, 5, 'INFORME MOROSOS - LISTADO CORTE', 0, 0, 'C');
		$pdf->setFontSize( 8);
		$pdf->Cell(15, 4, 'FECHA:');
		$pdf->Cell(10, 4, $fecha_generacion);
		$pdf->Ln();
		$pdf->Cell(65, 4, '', 0, 0, 'C');
		$pdf->setFontSize( 10);
		$pdf->Cell(124, 5, 'Al ' . $f_desde . ' con ' . $vdias . ' días en mora o más', 0, 0, 'C');
		$pdf->setFontSize( 8);
		$pdf->Cell(15, 4, 'USUARIO:');
		$pdf->Cell(1, 4, $pub_user3);
		$pdf->Ln();
		$pdf->Ln();
		$pdf->Ln();
	}

	$pdf->setFontSize( 7);
	$pdf->Cell(17, 4, 'NUMERO', 'LRT', 0, 'C');
	$pdf->Cell(20, 4, 'RUT', 'LRT', 0, 'C');
	$pdf->Cell(75, 4, 'NOMBRE DE CLIENTE', 'LRT', 0, 'C');
	$pdf->Cell(20, 4, 'NUMERO', 'LRT', 0, 'C');
	$pdf->Cell(20, 4, 'MONTO', 'LRT', 0, 'C');
	$pdf->Cell(20, 4, 'FECHA', 'LRT', 0, 'C');
	$pdf->Cell(20, 4, 'HORA', 'LRT', 0, 'C');
	$pdf->Cell(20, 4, 'FIRMA', 'LRT', 0, 'C');
	$pdf->Cell(0, 4, 'OBSERVACION', 'LRT', 0, 'C');
	$pdf->Ln();
	$pdf->Cell(17, 4, 'DE CUENTA', 'LRB', 0, 'C');
	$pdf->Cell(20, 4, '', 'LRB', 0, 'C');
	$pdf->Cell(75, 4, 'DIRECCIÓN DE CLIENTE', 'LRB', 0, 'C');
	$pdf->Cell(20, 4, 'DE MEDIDOR', 'LRB', 0, 'C');
	$pdf->Cell(20, 4, 'ADEUDADO', 'LRB', 0, 'C');
	$pdf->Cell(20, 4, 'DE CORTE', 'LRB', 0, 'C');
	$pdf->Cell(20, 4, 'DE CORTE', 'LRB', 0, 'C');
	$pdf->Cell(20, 4, '', 'LRB', 0, 'C');
	$pdf->Cell(0, 4, '', 'LRB', 0, 'C');
	$pdf->Ln();
	$pdf->setFontSize( 7);
}


$vdias=45;
$f_desde=date('d/m/Y');

$mcorte = 0;

$st_desde= substr($f_desde,6,4) .'-'. substr($f_desde,3,2) .'-'. substr($f_desde,0,2);
$bu_fecha = date("Y-m-d", strtotime("$st_desde -$vdias day"));
$fe_cons= $st_desde;


$info_pdf = array();

$sql = " SELECT * ";
$sql.= " from cbz_proceso_detalle pd ";
$sql.= " where  cpr_id  = '".$_GET["codigo"]."' ";
$res = $conx->query($sql)->fetchAll();
foreach($res as $item)
{
	$info = array();
	$info["numero"] =$item["cta_nro_cuenta"];
	$info["rut"] =$item["cta_rut"];
	$info["nombre"] =$item["cta_nombre"];
	$info["medidor"] =$item["cta_medidor"];
	$info["direccion"]= $item["cta_direccion"];
	$info["deuda"] = $item["cpr_deuda"];

	array_push($info_pdf , $info);
}

$Linea_Pag = 0;
$prox_pagina = 21;
agregar_pagina_pdf($pdf,$f_desde,$vdias,"USUARIO",date('d-m-Y H:i:s'),true);
foreach($info_pdf as $cnta)
{
	$Linea_Pag = $Linea_Pag + 1;
    if ($Linea_Pag >= $prox_pagina)
    {
    	// NUEVA HOJA
    	agregar_pagina_pdf($pdf,$f_desde,$vdias,"USUARIO",date('d-m-Y H:i:s'),false);
        $Linea_Pag = 1;
        $prox_pagina = 23;
    }

	// $pdf->Cell(20, 4, 'NUMERO', 'LRT', 0, 'C');
	// $pdf->Cell(20, 4, 'RUT', 'LRT', 0, 'C');
	// $pdf->Cell(75, 4, 'NOMBRE', 'LRT', 0, 'C');
	// $pdf->Cell(20, 4, 'NUMERO', 'LRT', 0, 'C');
	// $pdf->Cell(20, 4, 'MONTO', 'LRT', 0, 'C');
	// $pdf->Cell(20, 4, 'FECHA', 'LRT', 0, 'C');
	// $pdf->Cell(20, 4, 'HORA', 'LRT', 0, 'C');
	// $pdf->Cell(20, 4, 'FIRMA', 'LRT', 0, 'C');
	// $pdf->Cell(0, 4, 'OBSERVACION', 'LRT', 0, 'C');
	// $pdf->Ln();

	$pdf->writeHTMLCell(17, 4, '', '', agregar_padding_top($cnta["numero"]),'L',0,false,true,'R');
	$pdf->writeHTMLCell(20, 4, '', '', agregar_padding_top($cnta["rut"]), 'L',0,false,true,'R');
	$pdf->writeHTMLCell(75, 4, '', '', agregar_padding_top($cnta["nombre"],2), 'L');
	$pdf->writeHTMLCell(20, 4, '', '', agregar_padding_top($cnta["medidor"]), 'L',0,false,true,'R');
	$pdf->writeHTMLCell(20, 4, '', '', agregar_padding_top(number_format($cnta["deuda"], 0, ',', '.')), 'L',0,false,true,'R');
	$pdf->Cell(20, 4, '', 'L', 0, 'R');
	$pdf->Cell(20, 4, '', 'L', 0, 'R');
	$pdf->Cell(20, 4, '', 'L', 0, 'R');
	$pdf->Cell(0, 4, '', 'LR', 0, 'R');
    $pdf->Ln();

    $pdf->Cell(17, 4, '', 'LB', 0, 'R');
	$pdf->Cell(20, 4, '', 'LB', 0, 'R');
	$pdf->writeHTMLCell(75, 4, '', '', ($cnta["direccion"]), 'LB');
	$pdf->Cell(20, 4, '', 'LB', 0, 'R');
	$pdf->Cell(20, 4, '', 'LB', 0, 'R');
	$pdf->Cell(20, 4, '', 'LB', 0, 'R');
	$pdf->Cell(20, 4, '', 'LB', 0, 'R');
	$pdf->Cell(20, 4, '', 'LB', 0, 'R');
	$pdf->Cell(0, 4, '', 'LBR', 0, 'R');
    $pdf->Ln();

}
$pdf->Output("Cobranza" . date('d-m-Y H:i:s'));



?>